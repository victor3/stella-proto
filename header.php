<html>
<head>
	<title>Welcome to [Stella]<?php if ($id) { echo(' | '.ucfirst($id)); } ?></title>
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1">
	<script src="lib/js/jquery-2.1.3.min.js"></script>
	<link rel="stylesheet" href="lib/css/bootstrap.min.css">
	<script src="lib/js/bootstrap.min.js"></script>
	<script src="lib/js/repo.js"></script>
	<script src="lib/js/quill.min.js"></script>
    <script src="lib/js/owl.carousel.js"></script>
	<script src="lib/js/functions.js"></script>
	<link rel="stylesheet" href="lib/css/quill.base.css">
	<link rel="stylesheet" href="lib/css/owl.carousel.css">
	<link rel="stylesheet" href="lib/css/owl.theme.css">
	<link rel="stylesheet" href="lib/css/owl.transitions.css">
	<!-- <link rel="stylesheet" href="lib/css/quill.snow.css"> -->
	<link rel="stylesheet" href="styles/main.css">
	<link rel="stylesheet" href="styles/queries.css">
	<!--<script src="https://togetherjs.com/togetherjs-min.js"></script>-->
	<script>
		var logged = '<?php echo($logged); ?>';
		
		// Javascript to enable link to tab
		var hash = document.location.hash;
		var prefix = "tab_";
		if (hash) {
		    $('.nav-pills a#'+hash.replace(prefix,"")).tab('show');
		} 

		// Change hash for page-reload
		$('.nav-pills a').on('shown', function (e) {
		    window.location.hash = e.target.hash.replace("#", "#" + prefix);
		});
	</script>
</head>
<body>
	<?php if ($settings['debugging']) { ?>
	<div id="messages"></div>
	<?php } ?>
	<div class="container-fluid">
		
		<!-- HEADER -->
		<header id="header">
			
			<nav class="navbar">
				<div class="container">
					<div id="brand">
						<a class="brand-link navbar-brand" href="./">
							<img class="header-logo image-responsive pull-left" src="lib/img/Cisco_Logo_RGB_Screen_Blue2925.png" alt="Cisco Logo"> [Stella]
						</a>
					</div>
					<div class="navbar-header">
						<button id="navbar-toggle-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span style="background-color:#fff !important;" class="icon-bar"></span>
							<span style="background-color:#fff !important;" class="icon-bar"></span>
							<span style="background-color:#fff !important;" class="icon-bar"></span>
						</button>
					</div>
					<?php if ($logged) { ?>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-collapse-1">
						<ul class="nav sub-nav navbar-nav">
							<li class="menu-item"><a class="menu-link <?php if (strtolower($id)=='experiences') echo('selected'); ?>" href="./?id=experiences">Experiences</a></li>
							<li class="menu-item"><a class="menu-link <?php if (strtolower($id)=='resources') echo('selected'); ?>" href="./?id=resources">Resources</a></li>
							<li class="menu-item"><a class="menu-link <?php if (strtolower($id)=='discussions') echo('selected'); ?>" href="./?id=discussions">Discussions</a></li>
						</ul>
						<!-- navbar right -->
						<ul class="nav navbar-nav navbar-right">
							<!-- <li class="menu-item"><a class="menu-link <?php if (strtolower($id)=='experiences') echo('selected'); ?>" href="./?id=experiences">Experiences</a></li>
							<li class="menu-item"><a class="menu-link <?php if (strtolower($id)=='resources') echo('selected'); ?>" href="./?id=resources">Resources</a></li> -->
							<li class="menu-item dropdown">
								<a href="./?id=experiences" class="menu-link dropdown-toggle usermenu" data-toggle="dropdown" role="button" aria-expanded="false"><img class="img-circle avatar-big" src="lib/img/?32x32;avatar" alt="[Username]'s avatar">&nbsp;<!-- [username] --></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="./?id=account">Your Account</a></li>
									<li><a href="./?id=admin">Admin</a></li>
									<li role="presentation" class="divider"></li>
									<li><a href="./?out=1">Log Out</a></li>
								</ul>
							</li>
						</ul>
					</div><!-- .navbar-collapse -->
					<?php } ?>
				</div>
			</nav>
		</header>