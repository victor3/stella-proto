<?php

	//error_reporting(E_ALL);

	// kill cookie
	if (isset($_GET['out'])) {
		setcookie('stella-login', false, time() + (86400 * 365), "/"); // 86400 = 1 day
		header("Location: ./");
	}

	$logged = false;
	// cookied?
	$logged = $_COOKIE['stella-login'];
	
	/* GET LOAD PAGE */
	$id = (isset($_GET['id'])) ? $_GET['id'] : 'experiences';
	$section = (isset($_GET['section'])) ? '-'.$_GET['section'] : '';
	$filename = $id.$section;
	$content = (file_exists('sections/'.$filename.'.php')) ? $filename : 'login';
	if (!$logged) $content = 'login';
	
	/* SETTINGS */
	global $settings;
	$settings['debugging'] = true;
	$settings['show_jobs'] = false;
	$settings['show_updates'] = false;
	$settings['show_notifications'] = false;
	
	/* DATABASE */
	require_once('data/db.php');
	
	/* FUNCTIONS */
	require_once('lib/php/functions.php');

	// HEADER /////////////
	require_once('header.php');

?>

		<!-- CONTENT -->
		<div id="content">
			
			<?php include('sections/'.$content.'.php'); ?>
			
		</div><!-- #content -->
		
<?php 
	// FOOTER /////////////////////
	require_once('footer.php');
?>