<?php

/******************************************************
 * database *******************************************
 ****************************************************** */
$db = array(
	
	"stella" => array(
		"title" => "Welcome to [Stella]",
		"description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet.",
	),
	
	/******************************************************
	 * Experiences Database 
	 ****************************************************** */
	"experiences" => array(
		array(
			"id" => "EXPERIENCE-00000001",
			"name" => "Intro to IoE",
			"type" => "Online", 
			"hour_length" => "15",
			"image_ref" => "#049fd9",//"#004bb0",
			"image_url" => "images/prototype_single_experience_01.jpg",
			"description" => "Description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet...",
			"time-start" => "2015-02-08 07:00:00",
			"time-end" => "2015-09-15 21:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
				"course-0004",
			),
			"projects" => array(
				"PROJECT-00000001",
				"PROJECT-00000002",
				"PROJECT-00000003",
			),
		),
		array(
			"id" => "EXPERIENCE-00000002",
			"name" => "Day 1",
			"type" => "Workshop", 
			"hour_length" => "8",
			"image_ref" => "#049fd9",//"#0459ac",
			"image_url" => "images/prototype_single_experience_02.jpg",
			"description" => "Description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet...",
			"time-start" => "2015-02-08 07:00:00",
			"time-end" => "2015-09-15 21:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
			),
			"projects" => array(
				"PROJECT-00000002",
			),
		),
		array(
			"id" => "EXPERIENCE-00000003",
			"name" => "Connecting Things",
			"type" => "Online", 
			"hour_length" => "30",
			"image_ref" => "#049fd9",//"#096ea6",
			"image_url" => "images/prototype_single_experience_03.jpg",
			"description" => "Description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet...",
			"time-start" => "2015-02-08 07:00:00",
			"time-end" => "2015-09-15 21:00:00",
			"courses" => array(
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000003",
			),
		),
		array(
			"id" => "EXPERIENCE-00000004",
			"name" => "Connecting Data",
			"type" => "Online", 
			"hour_length" => "30",
			"image_ref" => "#049fd9",//"#0b7d9f",
			"image_url" => "images/prototype_single_experience_04.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000004",
			),
		),
		array(
			"id" => "EXPERIENCE-00000005",
			"name" => "Connecting People &amp; Processes",
			"type" => "Online", 
			"hour_length" => "30",
			"image_ref" => "#049fd9",//"#0e889d",
			"image_url" => "images/prototype_single_experience_05.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000005",
			),
		),
		array(
			"id" => "EXPERIENCE-00000006",
			"name" => "Rapid Prototyping",
			"type" => "Workshop", 
			"hour_length" => "30",
			"image_ref" => "#049fd9",//"#0d9098",
			"image_url" => "images/prototype_single_experience_06.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000006",
			),
		),
		array(
			"id" => "EXPERIENCE-00000007",
			"name" => "Hackathon 1",
			"type" => "Hackathon", 
			"hour_length" => "40",
			"image_ref" => "#049fd9",//"#119997",
			"image_url" => "images/prototype_single_experience_07.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
			),
			"projects" => array(
				"PROJECT-00000007",
			),
		),
		array(
			"id" => "EXPERIENCE-00000008",
			"name" => "IoE Fundamentals",
			"type" => "Workshop", 
			"hour_length" => "100",
			"image_ref" => "#049fd9",//"#12a593",
			"image_url" => "images/prototype_single_experience_08.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000008",
			),
		),
		array(
			"id" => "EXPERIENCE-00000009",
			"name" => "Mid-Program Assessment",
			"type" => "Online",
			"hour_length" => "8",
			"image_ref" => "#049fd9",//"#5fa094",
			"image_url" => "images/prototype_single_experience_09.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000009",
			),
		),
		/* JOB FAMILIES ******/
		array(
			"id" => "EXPERIENCE-00000010",
			"name" => "[Curated Job Family Specific]",
			"type" => "Online",
			"hour_length" => "200",
			"image_ref" => "#049fd9",//"#1aa393",
			"image_url" => "images/prototype_single_experience_10.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000009",
			),
		),
		array(
			"id" => "EXPERIENCE-00000011",
			"name" => "[Cisco Job Family Specific]",
			"type" => "Online",
			"hour_length" => "40",
			"image_ref" => "#049fd9",//"#608497",
			"image_url" => "images/prototype_single_experience_11.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000009",
			),
		),
		array(
			"id" => "EXPERIENCE-00000012",
			"name" => "Hackathon 2",
			"type" => "Workshop",
			"hour_length" => "40",
			"image_ref" => "#049fd9",//"#6a7399",
			"image_url" => "images/prototype_single_experience_12.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000009",
			),
		),
		array(
			"id" => "EXPERIENCE-00000013",
			"name" => "[Advanced Job Family Specific]",
			"type" => "Online",
			"hour_length" => "120",
			"image_ref" => "#049fd9",//"#76669b",
			"image_url" => "images/prototype_single_experience_13.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000009",
			),
		),
		array(
			"id" => "EXPERIENCE-00000014",
			"name" => "[Job Family Specific Jr. Consulting]",
			"type" => "In Person",
			"hour_length" => "200",
			"image_ref" => "#049fd9",//"#7c609b",
			"image_url" => "images/prototype_single_experience_14.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
			"projects" => array(
				"PROJECT-00000007",
				"PROJECT-00000008",
				"PROJECT-00000009",
			),
		),
		array(
			"id" => "EXPERIENCE-00000015",
			"name" => "Final Assessment for Certificate",
			"type" => "Online",
			"hour_length" => "8",
			"image_ref" => "#049fd9",//"#8d579f",
			"image_url" => "images/prototype_single_experience_15.jpg",
			"description" => "Description of experience goes here",
			"time-start" => "2015-01-01 00:00:00",
			"time-end" => "2015-01-01 00:00:00",
			"courses" => array(
				"course-0001",
				"course-0002",
				"course-0003",
			),
		),
	),
	
	
	/******************************************************
	 * PROJECTS
	 ****************************************************** */
	"projects" => array(
		
		"PROJECT-00000001" => array(
			"id" => "PROJECT-00000001",
			"pid" => "simple-keyboard-project",
			"name" => "Simple Keyboard Project",
			"description" => "This example shows how to use the tone() command to generate different pitches depending on which sensor is pressed.",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => "",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
		
		"PROJECT-00000002" => array(
			"id" => "PROJECT-00000002",
			"pid" => "onepage-scroll",
			"name" => "Onepage Scroll",
			"description" => "Create an Apple-like one page scroller website (iPhone 5S website) with One Page Scroll plugin",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Juan",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Jose",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Luis",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => array(
				"name" => "Rockwell Automation",
				"icon" => "lib/img/rockwell.jpg",
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
		
		"PROJECT-00000003" => array(
			"id" => "PROJECT-00000003",
			"pid" => "php-login-one-file",
			"name" => "PHP Login One File",
			"description" => "A simple, but secure PHP login script in one file and a flat-file SQLite database. No installation needed, ready to go in under 60seconds.",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => array(
				"name" => "Cisco",
				"icon" => "lib/img/cisco.png",
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
		
		"PROJECT-00000004" => array(
			"id" => "PROJECT-00000004",
			"pid" => "frwrdnet.github.io",
			"name" => "frwrd.net pages",
			"description" => "frwrd.net pages on github",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => "",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
		
		"PROJECT-00000005" => array(
			"id" => "PROJECT-00000005",
			"pid" => "frwrd-test",
			"name" => "frwrd test",
			"description" => "test repo",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => "",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
		
		"PROJECT-00000006" => array(
			"id" => "PROJECT-00000006",
			"pid" => "FlappyBirdTemplate-Spritebuilder",
			"name" => "Flappy Bird Template - Sprite Builder Project",
			"description" => "",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => array(
				"name" => "Cisco",
				"icon" => "lib/img/cisco.png",
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
		
		"PROJECT-00000007" => array(
			"id" => "PROJECT-00000007",
			"pid" => "discourse",
			"name" => "Discourse",
			"description" => "A platform for community discussion. Free, open, simple.",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => "",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
		
		"PROJECT-00000008" => array(
			"id" => "PROJECT-00000008",
			"pid" => "project-8",
			"name" => "Project 8",
			"description" => "This is the description of Project 8.",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => "",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
		
		"PROJECT-00000009" => array(
			"id" => "PROJECT-00000009",
			"pid" => "project-9",
			"name" => "Project #9",
			"description" => "This is the description of Project 9.",
			"github_user" => "frwrdnet",
			"team" => array(
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
				array(
					"user" => "Victor",
					"github_user" => "frwrdnet",
				),
			),
			"created_by" => "Victor",
			"sponsor" => "",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00",
			"wiki_timestamp" => "2015-02-08 07:00:00",
			"wiki" => "",
		),
	),
	
	
	/******************************************************
	 * DISCUSSIONS 
	 ****************************************************** */
	"discussions" => array(
		array(
			"id" => "DISCUSSIONS-00000001",
			"name" => "Physical Computing",
			"description" => "Description of this discussions category goes here",
			"members" => "12",
			"discussions" => "4",
			"recent_post" => array(
				"username" => "James",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "DISCUSSIONS-00000002",
			"name" => "Hardware Hackers",
			"description" => "Description of this discussions category goes here",
			"members" => "21",
			"discussions" => "7",
			"recent_post" => array(
				"username" => "Stella",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "DISCUSSIONS-00000003",
			"name" => "Network Programming Discussions",
			"description" => "Description of this discussions category goes here",
			"members" => "7",
			"discussions" => "12",
			"recent_post" => array(
				"username" => "Rebekah",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "DISCUSSIONS-00000004",
			"name" => "Tech Tools and Trades",
			"description" => "Description of this discussions category goes here",
			"members" => "18",
			"discussions" => "5",
			"recent_post" => array(
				"username" => "James",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "DISCUSSIONS-00000005",
			"name" => "Programmer's Den",
			"description" => "Description of this discussions category goes here",
			"members" => "13",
			"discussions" => "11",
			"recent_post" => array(
				"username" => "Phil",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "DISCUSSIONS-00000006",
			"name" => "SysAdmin's Space",
			"description" => "Description of this discussions category goes here",
			"members" => "31",
			"discussions" => "24",
			"recent_post" => array(
				"username" => "Dana",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
	),
	
	
	/******************************************************
	 * THREADS 
	 ****************************************************** */
	"threads" => array(
		array(
			"id" => "THREAD-00000001",
			"parent_id" => array(
				"DISCUSSIONS-00000002",
				"DISCUSSIONS-00000006",
			),
			"name" => "Are you a beginner to arduino?",
			"members" => "12",
			"replies" => "32",
			"recent_post" => array(
				"username" => "James",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"started_by" => "Victor",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "THREAD-00000002",
			"parent_id" => array(
				"DISCUSSIONS-00000001",
				"DISCUSSIONS-00000002",
				"DISCUSSIONS-00000003",
				"DISCUSSIONS-00000006",
			),
			"name" => "Best computer for the program",
			"members" => "4",
			"replies" => "9",
			"recent_post" => array(
				"username" => "Stella",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"started_by" => "Deepak",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "THREAD-00000003",
			"parent_id" => array(
				"DISCUSSIONS-00000002",
				"DISCUSSIONS-00000003",
				"DISCUSSIONS-00000006",
			),
			"name" => "[Long Thread Name Goes Here]",
			"members" => "15",
			"replies" => "10",
			"recent_post" => array(
				"username" => "Rebekah",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"started_by" => "Beba",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "THREAD-00000004",
			"parent_id" => array(
				"DISCUSSIONS-00000001",
				"DISCUSSIONS-00000003",
				"DISCUSSIONS-00000005",
			),
			"name" => "[Very Very Long Thread Name Goes Here]",
			"members" => "9",
			"replies" => "10",
			"recent_post" => array(
				"username" => "James",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"started_by" => "Lea",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "THREAD-00000005",
			"parent_id" => array(
				"DISCUSSIONS-00000003",
				"DISCUSSIONS-00000004",
				"DISCUSSIONS-00000005",
				"DISCUSSIONS-00000006",
			),
			"name" => "[Short Thread Name]",
			"members" => "7",
			"replies" => "10",
			"recent_post" => array(
				"username" => "Phil",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"started_by" => "Olmedo",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
		array(
			"id" => "THREAD-00000006",
			"parent_id" => array(
				"DISCUSSIONS-00000002",
				"DISCUSSIONS-00000004",
				"DISCUSSIONS-00000006",
			),
			"name" => "[Thread Name Goes Here]",
			"members" => "4",
			"replies" => "10",
			"recent_post" => array(
				"username" => "Dana",
				"content" => "I do not think we can attach the Arduino MP to the relay, it will blow it probably.",
				"timestamp" => "2015-02-08 07:00:00",
				"uid" => "SpaceName/DiscussionName/PostID"
			),
			"recent_users" => array(
				array(
					"username" => "username1",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username2",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username3",
					"avatar" => "lib/img/?36x36;666;avatar"
				),
				array(
					"username" => "username4",
					"avatar" => "lib/img/?36x36;666;avatar"
				)
			),
			"started_by" => "Rashida",
			"time_created" => "2015-02-08 07:00:00",
			"time_updated" => "2015-09-15 21:00:00"
		),
	
	),
	
	
	/******************************************************
	 * Thread Database 
	 ****************************************************** */
	"thread" => array(
		array(
			"id" => "REPLY-00000001",
			"username" => "James",
			"avatar" => "lib/img/?36x36;666;avatar",
			"content" => "The course introduces the concept of a network foundation connecting billions of things and trillions of gigabytes of data to enhance our decision making and interactions. Course modules describe how IOE drives the convergence between an organization's operational technology (OT) and information technology (IT) systems, the business processes for evaluating a problem and implementing an IoE solution, and the machine-to-machine (M2M), machine-to-people (M2P), and people-to-people (P2P) connections in an IoE solution.<br>
			[file:lib/img/?24x24;333;PDF]<br>
			[file:lib/img/?24x24;333;PDF]",
			"timestamp" => "2015-02-08 07:00:00",
			"uid" => "PostID",
		),
		array(
			"id" => "REPLY-00000002",
			"username" => "Andrea",
			"avatar" => "lib/img/?36x36;666;avatar",
			"content" => "The course introduces the concept of a network foundation connecting billions of things and trillions of gigabytes of data to enhance our decision making and interactions. Course modules describe how IOE drives the convergence between an organization's operational technology (OT) and information technology (IT) systems, the business processes for evaluating a problem and implementing an IoE solution, and the machine-to-machine (M2M), machine-to-people (M2P), and people-to-people (P2P) connections in an IoE solution.<br>
			[file:lib/img/?24x24;333;PDF]<br>
			[file:lib/img/?24x24;333;PDF]",
			"timestamp" => "2015-02-08 07:00:00",
			"uid" => "PostID",
		),
		array(
			"id" => "REPLY-00000002",
			"username" => "Andrea",
			"avatar" => "lib/img/?36x36;666;avatar",
			"content" => "<p>The Introduction to the Internet of Everything course provides an overview of the concepts and challenges of the transformational IoE economy.</p>
			<p class=\"attachment-inline\">
				<!--<a href=\"#\"><img src=\"lib/img/?24x24;333;PDF\" alt=\"\"> File_XXX.pdf</a>-->
				<span class=\"attachment-icon\">
					<a href=\"#\"><img class=\"img-thumbnail\" src=\"lib/img/?24x24;ccc;PDF\"></a>
				</span>
				<span class=\"attachment-filename\">
					<strong><a href=\"#\">File XXX</a></strong><br/>
					<small><a href=\"#\">File_XXX.pdf</a></small>
				</span>
			</p>
			<p>The course discusses the Internet and its evolution to the interconnection of people, processes, data, and things that forms the Internet of Everything.</p>
			<pre>
function main()
{
	ipc.ipcManager().thisInstance().registerEvent(\"messageReceived\", null, onMessage);
}

function cleanUp()
{
}

function onMessage(src, args)
{
	var params = args.message.toString().split(\",\");
	if (params.length != 3)
		return;

	var inventory = JSON.parse($getData(\"software.json\"));
	if (params[0].indexOf(\"ADD_SOFTWARE\") == 0)
	{
		var model = new Object();
		model.name = params[1];
		model.page = params[2];
		var bAdded = false;
		for (var i=0; i<inventory.length; i++)
		{
			if (inventory[i].name == model.name)
			{
				inventory[i] = model;
				bAdded = true;
				break;
			}
		}

		if (!bAdded)
			inventory.push(model);
	}

	var inventoryStr = JSON.stringify(inventory);
	if (inventoryStr.length > 0)
		$putData(\"software.json\", inventoryStr);
}
			</pre>
			<p>The course introduces the concept of a network foundation connecting billions of things and trillions of gigabytes of data to enhance our decision making and interactions. Course modules describe how IOE drives the convergence between an organization's operational technology (OT) and information technology (IT) systems, the business processes for evaluating a problem and implementing an IoE solution, and the machine-to-machine (M2M), machine-to-people (M2P), and people-to-people (P2P) connections in an IoE solution.</p>
			<p class=\"attachment-inline\">
				<!--<a href=\"#\"><img src=\"lib/img/?24x24;333;PDF\" alt=\"\"> File_XXX.pdf</a>-->
				<span class=\"attachment-icon\">
					<a href=\"#\"><img class=\"img-thumbnail\" src=\"lib/img/?24x24;ccc;PDF\"></a>
				</span>
				<span class=\"attachment-filename\">
					<strong><a href=\"#\">File XXX</a></strong><br/>
					<small><a href=\"#\">File_XXX.pdf</a></small>
				</span>
			</p>
			<p class=\"attachment-inline\">
				<!--<a href=\"#\"><img src=\"lib/img/?24x24;333;PDF\" alt=\"\"> File_XXX.pdf</a>-->
				<span class=\"attachment-icon\">
					<a href=\"#\"><img class=\"img-thumbnail\" src=\"lib/img/?24x24;ccc;PDF\"></a>
				</span>
				<span class=\"attachment-filename\">
					<strong><a href=\"#\">File XXX</a></strong><br/>
					<small><a href=\"#\">File_XXX.pdf</a></small>
				</span>
			</p>",
			/*"content" => "The Introduction to the Internet of Everything course provides an overview of the concepts and challenges of the transformational IoE economy.<br>
			[{file:{lib/img/?24x24;333;PDF}}]<br>
			The course discusses the Internet and its evolution to the interconnection of people, processes, data, and things that forms the Internet of Everything.<br>
			[{code:{
				function main()
				{
					ipc.ipcManager().thisInstance().registerEvent(\"messageReceived\", null, onMessage);
				}
 
				function cleanUp()
				{
				}
 
				function onMessage(src, args)
				{
					var params = args.message.toString().split(\",\");
					if (params.length != 3)
						return;
 
					var inventory = JSON.parse($getData(\"software.json\"));
					if (params[0].indexOf(\"ADD_SOFTWARE\") == 0)
					{
						var model = new Object();
						model.name = params[1];
						model.page = params[2];
						var bAdded = false;
						for (var i=0; i<inventory.length; i++)
						{
							if (inventory[i].name == model.name)
							{
								inventory[i] = model;
								bAdded = true;
								break;
							}
						}
 
						if (!bAdded)
							inventory.push(model);
					}
 
					var inventoryStr = JSON.stringify(inventory);
					if (inventoryStr.length > 0)
						$putData(\"software.json\", inventoryStr);
				}
			}}]
			<br>
			The course introduces the concept of a network foundation connecting billions of things and trillions of gigabytes of data to enhance our decision making and interactions. Course modules describe how IOE drives the convergence between an organization's operational technology (OT) and information technology (IT) systems, the business processes for evaluating a problem and implementing an IoE solution, and the machine-to-machine (M2M), machine-to-people (M2P), and people-to-people (P2P) connections in an IoE solution.<br>
			[file:lib/img/?24x24;333;PDF]<br>
			[file:lib/img/?24x24;333;PDF]",*/
			"timestamp" => "2015-02-08 07:00:00",
			"uid" => "PostID",
			"attachments" => array(
				array(
					"name" => "[File Name Goes Here]",
					"filename" => "Filename.pdf",
					"icon" => "lib/img/?36x36;666;avatar"
				),
			),
		),
		array(
			"id" => "REPLY-00000003",
			"username" => "Mark",
			"avatar" => "lib/img/?36x36;666;avatar",
			"content" => "The Introduction to the Internet of Everything course provides an overview of the concepts and challenges of the transformational IoE economy. The course discusses the Internet and its evolution to the interconnection of people, processes, data, and things that forms the Internet of Everything.
 
The course introduces the concept of a network foundation connecting billions of things and trillions of gigabytes of data to enhance our decision making and interactions. Course modules describe how IOE drives the convergence between an organization's operational technology (OT) and information technology (IT) systems, the business processes for evaluating a problem and implementing an IoE solution, and the machine-to-machine (M2M), machine-to-people (M2P), and people-to-people (P2P) connections in an IoE solution.",
			"timestamp" => "2015-02-08 07:00:00",
			"uid" => "PostID",
			"attachments" => array(
				array(
					"name" => "[File Name Goes Here]",
					"filename" => "Filename.pdf",
					"icon" => "lib/img/?36x36"
				),
			),
		),
		array(
			"id" => "REPLY-00000004",
			"username" => "Andrea",
			"avatar" => "lib/img/?36x36;666;avatar",
			"content" => "The Introduction to the Internet of Everything course provides an overview of the concepts and challenges of the transformational IoE economy. The course discusses the Internet and its evolution to the interconnection of people, processes, data, and things that forms the Internet of Everything.
 
The course introduces the concept of a network foundation connecting billions of things and trillions of gigabytes of data to enhance our decision making and interactions. Course modules describe how IOE drives the convergence between an organization's operational technology (OT) and information technology (IT) systems, the business processes for evaluating a problem and implementing an IoE solution, and the machine-to-machine (M2M), machine-to-people (M2P), and people-to-people (P2P) connections in an IoE solution.",
			"timestamp" => "2015-02-08 07:00:00",
			"uid" => "PostID",
			"attachments" => array(
				array(
					"name" => "[File Name Goes Here]",
					"filename" => "Filename.pdf",
					"icon" => "lib/img/?36x36"
				),
				array(
					"name" => "[File Name Goes Here]",
					"filename" => "Filename.jpg",
					"icon" => "lib/img/?36x36"
				),
				array(
					"name" => "[File Name Goes Here]",
					"filename" => "Filename.ppt",
					"icon" => "lib/img/?36x36"
				),
			),
			"gist" => array(
				array(
					"username" => "[username]",
					"project_name" => "[ProjectName]",
					"project_description" => "[GitHub Repository Description]",
					"icon" => "lib/img/?36x36",
					"gist_code" => "CONTENT GOES HERE",
				),
			),
		),
	),
	
	
	/******************************************************
	 * SERVICES
	 **********************************************************/
	"services" => array(
		array(
			"name" => "Cisco DevNet",
			"description" => "Cisco DevNet Service",
			"domain" => "cisco",
			"icon" => "lib/img/cisco.png",
			"url" => "http://developer.cisco.com",
		),
		array(
			"name" => "Cisco Packet Tracer",
			"description" => "Cisco Packet Tracer Service",
			"domain" => "cisco",
			"icon" => "lib/img/cisco.png",
			"url" => "http://www.ciscopackettracer.com",
		),
		array(
			"name" => "Cisco WebEx",
			"description" => "Cisco WebEx Service",
			"domain" => "cisco",
			"icon" => "lib/img/cisco.png",
			"url" => "http://www.wyliodrin.com",
		),
		array(
			"name" => "Cisco Spark",
			"description" => "Cisco Spark Service",
			"domain" => "cisco",
			"icon" => "lib/img/cisco.png",
			"url" => "http://web.ciscospark.com",
		),
		array(
			"name" => "GitHub",
			"description" => "GitHub Service",
			"domain" => "external",
			"icon" => "lib/img/?80x80;CCC;GitHub",
			"url" => "http://www.wyliodrin.com",
		),
		array(
			"name" => "Wyliodrin",
			"description" => "Wyliodrin Service",
			"domain" => "external",
			"icon" => "lib/img/?80x80;#CCC;Wyliodrin",
			"url" => "http://www.wyliodrin.com",
		),
		array(
			"name" => "Arduino Forums",
			"description" => "Arduino Forums Service",
			"domain" => "external",
			"icon" => "lib/img/?80x80;#CCC;Arduino",
			"url" => "http://www.arduino.cc",
		),
		array(
			"name" => "Raspberry Pi Forums",
			"description" => "Raspberry Pi Forums Service",
			"domain" => "external",
			"icon" => "lib/img/?80x80;#CCC;RasPi",
			"url" => "http://www.raspberrypi.com",
		),
	),
	
	/******************************************************
	 * COURSES
	 **********************************************************/
	"courses" => array(
		"course-0001" => array(
			"name" => "Course 0001",
			"description" => "Description of Course 0001",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0002" => array(
			"name" => "Course 0002",
			"description" => "Description of Course 0002",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0003" => array(
			"name" => "Course 0003",
			"description" => "Description of Course 0003",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0004" => array(
			"name" => "Course 0004",
			"description" => "Description of Course 0004",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0005" => array(
			"name" => "Course 0005",
			"description" => "Description of Course 0005",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0006" => array(
			"name" => "Course 0006",
			"description" => "Description of Course 0006",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0007" => array(
			"name" => "Course 0007",
			"description" => "Description of Course 0007",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0008" => array(
			"name" => "Course 0008",
			"description" => "Description of Course 0008",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0009" => array(
			"name" => "Course 0009",
			"description" => "Description of Course 0009",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0010" => array(
			"name" => "Course 0010",
			"description" => "Description of Course 0010",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0011" => array(
			"name" => "Course 0011",
			"description" => "Description of Course 0011",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0012" => array(
			"name" => "Course 0012",
			"description" => "Description of Course 0012",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0013" => array(
			"name" => "Course 0013",
			"description" => "Description of Course 0013",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0014" => array(
			"name" => "Course 0014",
			"description" => "Description of Course 0014",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
		"course-0015" => array(
			"name" => "Course 0015",
			"description" => "Description of Course 0014",
			"url" => "https://www.coursera.org/course/pythonlearn",
		),
	),
	
	/******************************************************
	 * ACTIVITIES
	 **********************************************************/
	"activities" => array(
		array(
			"type" => "reply",
			"user" => "Jared",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "message",
			"user" => "Dan",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "reply",
			"user" => "Patrick",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "comment",
			"user" => "Leela",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "comment",
			"user" => "Dana",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "reply",
			"user" => "Chris",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "message",
			"user" => "Katie",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "reply",
			"user" => "Maaloufa",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "reply",
			"user" => "Mia",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "reply",
			"user" => "Carla",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "comment",
			"user" => "Milo",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "comment",
			"user" => "Beatrix",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "message",
			"user" => "Mayana",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "reply",
			"user" => "Wonhee",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
		array(
			"type" => "reply",
			"user" => "Laura",
			"timestamp" => "2015-05-12 00:00:00",
			"content" => "Activity content goes here",
		),
	),
	
	
	/******************************************************
	 * JOBS 
	****************************************************** */
	"jobs" => array(
		array(
			"id" => "JOB-00000001",
			"title" => "Data Analyst",
			"company" => "Honeywell", 
			"description" => "Description of job goes here",
			"location" => "San Jose, CA",
			"date-added" => "2015-01-01 00:00:00",
			"date-expiration" => "2015-01-01 00:00:00"
		),
		array(
			"id" => "JOB-00000002",
			"title" => "Field Engineer",
			"company" => "Honeywell", 
			"description" => "Description of job goes here",
			"location" => "San Jose, CA",
			"date-added" => "2015-01-01 00:00:00",
			"date-expiration" => "2015-01-01 00:00:00"
		),
		array(
			"id" => "JOB-00000003",
			"title" => "Data Analyst",
			"company" => "Honeywell", 
			"description" => "Description of job goes here",
			"location" => "San Jose, CA",
			"date-added" => "2015-01-01 00:00:00",
			"date-expiration" => "2015-01-01 00:00:00"
		),
		array(
			"id" => "JOB-00000004",
			"title" => "Field Engineer",
			"company" => "Honeywell", 
			"description" => "Description of job goes here",
			"location" => "San Jose, CA",
			"date-added" => "2015-01-01 00:00:00",
			"date-expiration" => "2015-01-01 00:00:00"
		),
		array(
			"id" => "JOB-00000005",
			"title" => "Field Engineer",
			"company" => "Honeywell", 
			"description" => "Description of job goes here",
			"location" => "San Jose, CA",
			"date-added" => "2015-01-01 00:00:00",
			"date-expiration" => "2015-01-01 00:00:00"
		),
		array(
			"id" => "JOB-00000006",
			"title" => "Data Analyst",
			"company" => "Honeywell", 
			"description" => "Description of job goes here",
			"location" => "San Jose, CA",
			"date-added" => "2015-01-01 00:00:00",
			"date-expiration" => "2015-01-01 00:00:00"
		)
	),
	
	
	/******************************************************
	 * CONVERSATIONS
	 ****************************************************** */
	"conversations" => array(
		array(
			"thread" => "[Community Thread]",
			"user" => "James",
			"comment" => "I don't think we can attach the Arduino MP to the relay, it'll blow it probably.",
			"timestamp" => "2015-03-01 00:00:00"
		),
		array(
			"thread" => "[Community Thread]",
			"user" => "Stella",
			"comment" => "I don't think we can attach the Arduino MP to the relay, it'll blow it probably.",
			"timestamp" => "2015-02-01 00:00:00"
		),
		array(
			"thread" => "[Community Thread]",
			"user" => "Phil",
			"comment" => "I don't think we can attach the Arduino MP to the relay, it'll blow it probably.",
			"timestamp" => "2015-01-01 00:00:00"
		),
		array(
			"thread" => "[Community Thread]",
			"user" => "Patricia",
			"comment" => "I don't think we can attach the Arduino MP to the relay, it'll blow it probably.",
			"timestamp" => "2015-01-01 00:00:00"
		),
		array(
			"thread" => "[Community Thread]",
			"user" => "Ryan",
			"comment" => "I don't think we can attach the Arduino MP to the relay, it'll blow it probably.",
			"timestamp" => "2015-01-01 00:00:00"
		)
	),
	
	/******************************************************
	 * NOTIFICATIONS 
	 ****************************************************** */
	"alerts" => array(
		array(
			"type" => "warning",
			"title" => "Hackathon coming this Monday",
			"timestamp" => "December 12, 2015"
		),
		array(
			"type" => "danger",
			"title" => "Assessment coming this Friday",
			"timestamp" => "December 15, 2015"
		)
	)
);
	
/******************************************************
 * LIPSUM PLACEHOLDER 
 ****************************************************** */

$lipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas pellentesque ligula eros, at auctor mi mollis a. Vivamus ut ornare orci. Donec non mollis eros. Aliquam fringilla consequat ante, sed condimentum ipsum tristique at. Integer vel turpis risus. Donec tempus porta tempor. Suspendisse et aliquam elit. Sed id pellentesque tortor, et scelerisque elit. Vestibulum odio urna, sagittis eu dignissim in, rhoncus ut ex. Cras in sodales lectus. Donec convallis vestibulum blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent tristique, erat at fringilla auctor, orci velit placerat turpis, condimentum lacinia justo nibh a purus. Suspendisse et sem eu turpis feugiat faucibus eu nec augue.

Aliquam erat volutpat. Sed condimentum pellentesque mi, et volutpat lorem lacinia rutrum. Donec ullamcorper, nisl quis semper mollis, risus leo semper velit, id tempus sapien diam sed massa. Praesent vehicula mauris et turpis ultricies, ultrices placerat nunc pulvinar. Donec sed commodo dolor. Fusce tincidunt ultrices eros id rutrum. Vivamus bibendum velit vitae nulla semper, non pretium tellus congue. Nunc dictum nulla ut nisl facilisis, lacinia tempor felis lobortis. Aenean interdum in leo nec posuere. Pellentesque at viverra nulla.

Nullam dolor odio, mollis ut nunc ac, vulputate tristique orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam erat volutpat. Ut condimentum in lectus sit amet ultrices. Nam tincidunt nulla vitae felis euismod, vitae tempus nisl lacinia. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent neque purus, iaculis eget mi eget, ultricies ornare sem. Duis eget risus arcu.

Sed consectetur nulla ut libero semper, quis eleifend elit viverra. Sed dapibus sagittis lorem pharetra hendrerit. Ut euismod, massa sit amet fermentum venenatis, justo lorem placerat risus, in suscipit turpis orci a velit. Sed hendrerit augue nec tincidunt auctor. Nam sed tempor dolor, ut porttitor nisl. Proin libero nisl, vehicula ut eros non, scelerisque faucibus lectus. Suspendisse potenti. Pellentesque viverra, mauris vel eleifend egestas, nunc leo malesuada felis, ac gravida risus nunc gravida odio. Nam iaculis dui urna, et pharetra eros porttitor et. Donec mollis lorem et elementum tincidunt.

Pellentesque feugiat turpis id leo convallis, in faucibus eros aliquet. In hac habitasse platea dictumst. Vivamus efficitur ante et tortor tempor pulvinar. Phasellus tortor est, fermentum sit amet sodales quis, blandit sed lectus. Integer pretium lacus quis mi condimentum congue. Proin ut justo ut mauris consequat molestie. Donec rhoncus justo ac ultricies lobortis. Praesent malesuada egestas mauris, ut placerat lacus ullamcorper ac. Etiam quis ornare nisl. In ullamcorper tincidunt gravida. Maecenas vehicula ante felis, eu ultrices sapien interdum eget. Morbi vitae commodo metus, nec congue lacus. Donec semper enim vel ante rutrum efficitur. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed feugiat gravida mattis.";

?>