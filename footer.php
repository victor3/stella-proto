		<!-- FOOTER -->
		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="footer-row">
							<h4>Service Menu</h4>
							<ul>
								<li><a href="./?id=admin">Admin</a></li>
								<li><a href="./?id=sitemap">Sitemap</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						
					</div>
				</div><!-- .row -->
				<div class="row">
					<div class="col-md-12">
						<div class="copyright">
							<p>Copyright &copy;2015 Cisco Systems. All rights reserved.</p>
						</div>
					</div>
				</div>
			</div><!-- .container -->
		</footer>
	</div><!-- container-fluid -->

	<!-- SPARK -->
	<div id="spark" class="container">
		<div id="sparkicon" class="icon">Spark</div>
		<div id="sparkbox" class="on">
			<div class="resize"><img src="lib/img/resize.png" alt="resize"/></div>
			<div class="close">&times;</div>
			<iframe src="https://web.ciscospark.com/#/rooms/38bacbf0-6ea6-11e4-ad18-09f4e8f62c78" id="sparkframe"></iframe>
		</div>
	</div>
</body>
</html>