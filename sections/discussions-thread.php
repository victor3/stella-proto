			<?php
				$parent = (isset($_GET['parent'])) ? $_GET['parent'] : '[Parent Name]';
				$name = (isset($_GET['name'])) ? $_GET['name'] : '[Campus Category Name]';
			?>
			
			<div class="container">
				<div class="breadcrumbs">
					Stella / 
					<a href="./?id=discussions">Discussions</a> / 
					<a href="./?id=discussions-category&name=<?php echo(urlencode($parent)); ?>"><?php echo($parent); ?></a> /
				</div>
				
				<!-- SPACES -->
				<div class="row">
					<div class="col-md-8">
						<h1 class="headline"><?php echo(urldecode($name)); ?></h1>
						<p class="description">Discussions:</p>
					</div>
					<div class="col-md-4">
						<div class="participants">
							<p>Participants</p>
							<?php //thread(6);
								global $db;
								$n = 9999;
								$n = ($n>count($db['thread'])) ? count($db['thread']) : $n;
								for ($i=0; $i<$n; $i++) {
									$thread = $db['thread'][$i];
									//$nn = count($thread['attachments']);
									?>
									<a class="thread-avatar" href="#"><img class="avatar" src="lib/img/?avatar" alt="<?php echo($thread['username']); ?>"></a>
							<?php } ?>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="thread col-xs-12">
						<?php
							for ($i=0; $i<$n; $i++) {
								$thread = $db['thread'][$i];
								$nn = count($thread['attachments']);
								?>
								<div id="thread-item-<?php echo($i); ?>" class="thread-item <?php if (($i+1)==$n) echo('border-bottom'); ?> <?php if($i===0) echo('first-reply'); ?> row">
									<div class="thread-content col-md-12">
										<div class="thread-content-header">
											<a class="thread-avatar" href="#"><img class="<?php if ($i===0) echo('avatar-big'); else echo('avatar') ?>" src="lib/img/?avatar"></a> <a href="#"><?php echo($thread['username']); ?></a> <small class="space-meta"> <?php echo(pretty($thread['timestamp'])); ?></small>
										</div>
										<div class="thread-comment">
											<?php echo($thread['content']); ?>
										</div>
									</div>
								</div>
							<?php
							}
						?>
						<div class="reply-area">
							<button class="reply-button btn-button btn-large">Leave a Reply</button>
							<div class="reply-form">
								<div class="thread-content-header">
									<a class="thread-avatar" href="#">
										<img class="img-circle avatar" src="lib/img/?avatar">
									</a> 
									<a href="#">[Your Username]</a> 
									<small class="space-meta"> right now</small>
								</div>
								<form class="row">
									<div class="col-md-8">
										<div class="form-group">
											<!-- quill editor -->
											<div id="content-container">
 									        <div id="editor-wrapper">
 									          <div id="formatting-container">
 									            <select title="Font" class="ql-font">
 									              <option value="sans-serif" selected>Sans Serif</option>
 									              <option value="Georgia, serif">Serif</option>
 									              <option value="Monaco, 'Courier New', monospace">Monospace</option>
 									            </select>
 									            <select title="Size" class="ql-size">
 									              <option value="10px">Small</option>
 									              <option value="13px" selected>Normal</option>
 									              <option value="18px">Large</option>
 									              <option value="32px">Huge</option>
 									            </select>
 									            <select title="Text Color" class="ql-color">
 									              <option value="rgb(255, 255, 255)">White</option>
 									              <option value="rgb(0, 0, 0)" selected>Black</option>
 									              <option value="rgb(255, 0, 0)">Red</option>
 									              <option value="rgb(0, 0, 255)">Blue</option>
 									              <option value="rgb(0, 255, 0)">Lime</option>
 									              <option value="rgb(0, 128, 128)">Teal</option>
 									              <option value="rgb(255, 0, 255)">Magenta</option>
 									              <option value="rgb(255, 255, 0)">Yellow</option>
 									            </select>
 									            <select title="Background Color" class="ql-background">
 									              <option value="rgb(255, 255, 255)" selected>White</option>
 									              <option value="rgb(0, 0, 0)">Black</option>
 									              <option value="rgb(255, 0, 0)">Red</option>
 									              <option value="rgb(0, 0, 255)">Blue</option>
 									              <option value="rgb(0, 255, 0)">Lime</option>
 									              <option value="rgb(0, 128, 128)">Teal</option>
 									              <option value="rgb(255, 0, 255)">Magenta</option>
 									              <option value="rgb(255, 255, 0)">Yellow</option>
 									            </select>
 									            <select title="Text Alignment" class="ql-align">
 									              <option value="left" selected>Left</option>
 									              <option value="center">Center</option>
 									              <option value="right">Right</option>
 									              <option value="justify">Justify</option>
 									            </select>
 									            <button title="Bold" class="ql-format-button ql-bold">Bold</button>
 									            <button title="Italic" class="ql-format-button ql-italic">Italic</button>
 									            <button title="Underline" class="ql-format-button ql-underline">Under</button>
 									            <button title="Strikethrough" class="ql-format-button ql-strike">Strike</button>
 									            <button title="Link" class="ql-format-button ql-link">Link</button>
 									            <button title="Image" class="ql-format-button ql-image">Image</button>
 									            <button title="Bullet" class="ql-format-button ql-bullet">Bullet</button>
 									            <button title="List" class="ql-format-button ql-list">List</button>
 									          </div>
 									          <div id="editor-container">
												<!-- textarea -->
												<textarea id="reply-textarea" class="reply-field form-control" rows="3"></textarea>
 									          </div>
 									        </div>
											</div>
										</div>
										<button type="submit" class="reply-submit-button btn btn-button btn-large">Post Reply</button>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<!-- <label for="replyAttachItem" class="reply-attach-doc-label">Attach Document</label> -->
											<p class="help-block reply-attach-doc-label">Attach a document to your reply (optional)</p>
											<input type="file" name="replyAttachItem" id="replyAttachItem" class="reply-attach-doc">
										</doc>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				
			</div><!-- container -->