			<?php
			
				$pages = array();
				$dir = scandir('./sections');
				foreach($dir as $item) {
					if ((strpos($item,".")!==0) && (strpos($item,"-")!==0) && $item!=='sitemap.php') {
						$name = str_replace(".php","",$item);
						$pages[$i]['filename'] = $name;
						$items = explode("-",$name);
						$pages[$i]['id'] = $items[0];
						$pages[$i]['section'] = $items[1];
					}
					$i++;
				}
				
			?>
						
			<div class="container">
				
				<!-- <div class="breadcrumbs">
					Stella /
				</div> -->
				
				<!-- Dashboard -->
				<h1 class="headline">Sitemap</h1>
				
				<div class="sitemap">
					<ul>
						<?php foreach($pages as $page) {
							$filename = $page['filename'];
							$url = "./?id=".$page['id'];
							if ($page['section']!='') {
								$url .= "&section=".$page['section'];
							}
						?>
						<li><a href="<?php echo($url); ?>"><p><?php echo($filename); ?></p></li>
						<?php } ?>
					</ul>
				</div>
					
			</div>
			
			