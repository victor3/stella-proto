			<div class="container">
				
				<div id="admin-sections">
					
					<!-- resources -->
					<div id="resources-box" class="section admin-section">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Resources <a class="btn btn-bottom pull-right" href="javascript:edit('resource','');">Add Resource</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th class="opt">Description</th>
										<th>URL</th>
										<th class="opt">Icon (URL)</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php
									global $db;
									$i = 0;
									foreach ($db['services'] as $svc) {
										$i++;
										?>
										<tr>
											<!-- <th><input type="checkbox" id="experience-<?php echo($svc['id']); ?>"></th> -->
											<td><strong><?php echo($i); ?></strong></td>
											<td><a href="<?php echo($svc['url']); ?>"><?php echo($svc['name']); ?></a></td>
											<td><?php echo($svc['description']); ?></td>
											<td><a href="<?php echo($svc['url']); ?>"><?php echo($svc['url']); ?><a href="#"></td>
											<td><img class="service-icon img-responsive" src="<?php echo($svc['icon']); ?>" alt="<?php echo($svc['name']); ?> Icon"></td>
											<td>
												<a class="btn btn-wire" href="javascript:edit('resource','<?php echo($exp['id']); ?>');">Edit</a>
												<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				
				</div>
				
			</div><!-- .container -->
			
			
			<!-- edit modal -->
			<div id="edit-modal" class="container-fluid">
				
				<!-- <a class="btn btn-space btn-wire pull-right" href="javascript:edit('close');">Close</a> -->
				
				<!-- container -->
				<div class="form-container">
					
					<!-- resource -->
					<div id="edit-resource-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Resource</button>
							</div>
						
							<h3><span></span> Resource</h3>
						
							<div class="form-group">
								<label for="ResourceName">Name</label>
								<input id="ResourceName" class="form-control" placeholder="Enter Resource Name" type="text" />
							</div>
							<div class="form-group">
								<label for="ResourceDescription">Description (optional)</label>
								<textarea id="ResourceDescription" class="form-control" placeholder="Enter Resource Description" rows="3"></textarea>
							</div>
							<div class="form-group">
								<label>
									<input type="checkbox" id="CiscoResource" /> Cisco Resource
								</label>
							</div>
							<div class="form-group">
								<label for="ResourceURL">URL</label>
								<input id="ResourceURL" class="form-control" placeholder="Enter Resource URL" type="text" />
							</div>
							<div class="form-group">
								<label for="ResourceIconURL">Icon (URL)</label>
								<input id="ResourceIconURL" class="form-control" placeholder="Enter Icon URL" type="text" />
							</div>
						</div>
					</div>
					
				</div><!-- container -->
				
				<!-- bg -->
				<div class="bg"></div>
				
			</div><!-- edit modal -->

