			<div class="container">
				
				<div id="admin-sections">
					
					<!-- cohorts -->
					<div id="cohorts-box" class="section admin-section">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Cohorts <a class="btn btn-bottom pull-right" href="javascript:edit('cohort','');">Add Cohort</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th>Place</th>
										<th>Tracks</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<!-- <th><input type="checkbox" id="cohort-1"></th> -->
										<td>1</td>
										<td><a href="#">Cohort 1</a></td>
										<td>San Francisco</td>
										<td>
											<div class="track">Track 1 (Default)</div>
											<div class="track">Track 2</div>
											<div class="track">Track 3</div>
										</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('cohort','cohort-1');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
									<tr>
										<!-- <th><input type="checkbox" id="cohort-2"></th> -->
										<td>2</td>
										<td><a href="#">Cohort 2</a></td>
										<td>Berlin</td>
										<td>
											<div class="track">Track 4 (Default)</div>
											<div class="track">Track 5 (Default)</div>
											<div class="track">Track 6</div>
										</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('cohort','cohort-2');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				
				</div>
				
			</div><!-- .container -->
			
			
			<!-- edit modal -->
			<div id="edit-modal" class="container-fluid">
				
				<!-- <a class="btn btn-space btn-wire pull-right" href="javascript:edit('close');">Close</a> -->
				
				<!-- container -->
				<div class="form-container">
					
					<!-- cohort -->
					<div id="edit-cohort-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Cohort</button>
							</div>
						
							<h3><span></span> Cohort</h3>
							
							<div class="form-group">
								<label for="CohortName">Cohort Name</label>
								<input id="CohortName" class="form-control" placeholder="Enter Cohort Name" type="text" />
							</div>
							<div class="form-group">
								<label for="CohortDescription">Cohort Description <small>(Optional)</small></label>
								<input id="CohortDescription" class="form-control" placeholder="Enter Cohort Description" type="text" />
							</div>
							<div class="form-group">
								<label class="wide">Tracks</label>
								<div class="row narrow">
									<div class="form-box col-md-4 col-sm-6">
										<label>
											<input type="checkbox" id="track-1" class="track-selector" /> Track 1
										</label>
										<small class="pull-right">(<input type="checkbox" id="track-1-default" /> Make default)</small>
									</div>
									<div class="form-box col-md-4 col-sm-6">
										<label>
											<input type="checkbox" id="track-2" class="track-selector" /> Track 2
										</label>
										<small class="pull-right">(<input type="checkbox" id="track-1-default" /> Make default)</small>
									</div>
									<div class="form-box col-md-4 col-sm-6">
										<label>
											<input type="checkbox" id="track-3" class="track-selector" /> Track 3
										</label>
										<small class="pull-right">(<input type="checkbox" id="track-1-default" /> Make default)</small>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
					
				</div><!-- container -->
				
				<!-- bg -->
				<div class="bg"></div>
				
			</div><!-- edit modal -->

