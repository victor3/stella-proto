			<?php
				global $db;
				$id = (isset($_GET['project'])) ? $_GET['project'] : 'project-0001';
				$project = $db['projects'][$id];
				$repo_code_link = $project['github_user'].'/'.$project['pid'];
				global $wiki_html;
				
				echo("
		<script>
			var repo_loaded = false;
			function load_repo(){
				if (!repo_loaded) {
					$('#repo-code').repo({ user: '".$project['github_user']."', name: '".$project['pid']."' });
					//$('#repo-gist').repo({ user: '".$project['github_user']."', name: '".$project['pid']."' });
					repo_loaded = true;
				}
			}
		</script>");
			?>
			
			<div class="container">
				
				<div class="breadcrumbs">
					<a href="./">Stella</a> / 
					<a href="./?id=experiences">Experiences</a> / 
				
					<?php if($project['sponsor']!='') { ?>
					<div id="sponsor">
						<div class="sponsor-name">
							<small>Sponsored by</small>
							<?php echo($project['sponsor']['name']); ?>
						</div>
						<div class="sponsor-image">
							<img src="<?php echo($project['sponsor']['icon']); ?>" alt="<?php echo($project['sponsor']['name']); ?>">
						</div>
					</div>
					<?php } ?>
				</div>
				
				<!-- PROJECT -->
				<div id="<?php echo($project['id']); ?>" class="project">
					
					<div class="project-header">
						
						<div class="row">
							<div class="col-md-9">
								<h1 class="headline"><?php echo($project['name']); ?> <small>Project</small></h1>
								<p class="description"><?php echo($project['description']); ?></p>
							</div>
							<div class="col-md-3">
								<div class="team-members">
									<h4>Team Members</h4>
									<div class="team-member-icons">
									<?php foreach($project['team'] as $member) { ?>
										<img class="avatar" src="lib/img/?avatar" alt="">
									<?php } ?>
									</div>
								</div><!-- team members -->
							</div><!-- col -->
						</div><!-- row -->
						
						<nav id="project-sections-nav" role="navigation">
							<ul id="project-sections-menu" class="nav nav-tabs">
								<li id="wiki-menu" role="presentation" class="active"><a href="javascript:show_section('wiki','project-sections');">Wiki</a></li>
								<li id="repo-menu" role="presentation"><a href="javascript:show_section('repo','project-sections');load_repo();">Repository</a></li>
							</ul>
						</nav>
						
					</div>
					
					<div id="project-sections">
					
						<!-- <div class="project-content col-md-7"> -->
						<div id="wiki-box" class="section">
							
							<div class="row">
								<div class="col-md-9">
									<!-- <h3>Project Wiki</h3> -->
									<a id="wiki-edit-btn" class="btn btn-wire btn-top btn-default pull-right" href="#">Edit Page</a>
									<h2 class="wiki-page-title">Wiki Page 0001</h1>
									<div id="wiki-html">
										
										<div id="wiki-html-content">
											<?php echo get_wiki_page($project['id'],'wiki-page-0001'); ?>
										</div>
										
										<div class="wiki-reply-area">
											<button class="reply-button btn-button btn-large">Leave a Reply</button>
											<div class="reply-form">
												<div class="project-comment-header">
													<a class="project-avatar" href="#">
														<img class="avatar" src="lib/img/?avatar">
													</a> 
													<a href="#">[Your Username]</a> 
													<small class="space-meta"> right now</small>
												</div>
												<form class="row">
													<div class="col-md-8">
														<div class="form-group">
															<textarea class="reply-field form-control" rows="3"></textarea>
														</div>
														<button type="submit" class="reply-submit-button btn btn-button btn-large">Post Reply</button>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label for="replyAttachItem" class="reply-attach-doc-label">Attach Document</label>
														
															<input type="file" name="replyAttachItem" id="replyAttachItem" class="reply-attach-doc">
														</div>
													</div>
												</form>
											</div>
										</div><!-- .reply-area -->
									</div><!-- wiki page -->
									
									<div id="wiki-edit">
										<h4>Edit Project Wiki</h4>
										<textarea id="wiki-edit-textarea" class="form-control">
											<?php echo get_wiki_page($project['id'],'wiki-page-0001'); ?>
										</textarea>
										<button id="wiki-save" class="btn btn-button btn-top btn-large">Save</button>
										
									</div><!-- wiki edit -->
								</div><!-- col -->
								
								<!-- wiki-pages -->
								<div class="col-md-3">
									<div class="wiki-pages-menu">
										<h3 class="pull-left">Pages</h3>
										<a class="btn btn-lg repo-link btn-top btn-wire btn-default pull-right" href="#">Add Page</a>
										<ul class="pull-left">
										<?php
										$pages = get_wiki_menu($project['id']);
										foreach( $pages as $page ) {
											$page_id = str_replace('.txt','',$page);
											$page_name = ucwords(str_replace('-',' ',$page_id));
										?>
											<li><a href="<?php echo($page_id); ?>"><?php echo($page_name); ?></a></li>
										<?php } ?>
										</ul>
									</div><!-- wiki-pages menu -->
								</div><!-- col-3 -->
								
							</div><!-- row -->
						</div><!-- wiki-box -->
								
						<div id="repo-box" class="section hidden">
							<!-- project repo -->
							<h3 class="pull-left">Project Repository</h3>
							<a class="btn btn-lg repo-link btn-top btn-wire btn-default pull-right" href="<?php echo('https://github.com/'.$repo_code_link); ?>">View on GitHub</a>
							<div id="repo-code" class="repo-code"></div>
						</div><!-- repo-box -->
					
					</div><!-- project-sections -->
				</div><!-- #project-id -->
			</div><!-- container -->