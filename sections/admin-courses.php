			<div class="container">
				
				<div id="admin-sections">
					
					<!-- courses -->
					<div id="courses-box" class="section admin-section">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Courses <a class="btn btn-bottom pull-right" href="javascript:edit('course','');">Add Course</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th>URL</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php
									global $db;
									$i = 0;
									foreach ($db['courses'] as $course) {
										$i++;
										?>
										<tr>
											<!-- <th><input type="checkbox" id="course-<?php echo($i); ?>"></th> -->
											<td><strong><?php echo($i); ?></strong></td>
											<td><a href="<?php echo($course['url']); ?>"><?php echo($course['name']); ?></a></td>
											<td><div class="truncate"><a href="<?php echo($course['url']); ?>"><?php echo($course['url']); ?></a></div></td>
											<td>
												<a class="btn btn-wire" href="javascript:edit('course','course-<?php echo($course['id']); ?>');">Edit</a>
												<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				
				</div>
				
			</div><!-- .container -->
			
			
			<!-- edit modal -->
			<div id="edit-modal" class="container-fluid">
				
				<!-- <a class="btn btn-space btn-wire pull-right" href="javascript:edit('close');">Close</a> -->
				
				<!-- container -->
				<div class="form-container">
					
					<!-- course -->
					<div id="edit-course-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Course</button>
							</div>
						
							<h3><span></span> Course</h3>
							
							<div class="form-group">
								<label for="CourseName">Course Name</label>
								<input id="CourseName" class="form-control" placeholder="Enter Course Name" type="text" />
							</div>
							<div class="form-group">
								<label for="CourseDescription">Course Description</label>
								<input id="CourseDescription" class="form-control" placeholder="Enter Course Description" type="text" />
							</div>
							<div class="form-group">
								<label for="CourseURL">Course URL</label>
								<input id="CourseURL" class="form-control" placeholder="Enter Course URL" type="text" />
							</div>
						</div>
					</div>
					
				</div><!-- container -->
				
				<!-- bg -->
				<div class="bg"></div>
				
			</div><!-- edit modal -->

