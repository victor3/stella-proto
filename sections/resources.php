			<div class="container">
				<div class="breadcrumbs">
					Stella /
				</div>
				
				<?php
				global $db;
				$services = array();
				$domains = array();
				foreach($db['services'] as $service) {
					if (!in_array($service['domain'],$domains)) $domains[] = $service['domain'];
					$services[$service['domain']][] = $service;
				}
				?>

				<h1>Resources</h1>

				<div class="row">
					
					<!-- main area -->
					<div class="col-md-8">
						
						<!-- cisco services area -->
						<?php foreach($domains as $domain) { ?>
						<div class="services-area">
							<h3><?php echo(ucwords($domain)); ?> Services</h3>
							<div class="row narrow">
								<?php foreach($services[$domain] as $service) { ?>
								<div class="service-item col-md-3 col-sm-4 col-xs-6">
									<a href="<?php echo($service['url']); ?>">
										<img class="img-responsive" src="<?php echo(hex($service['icon'])); ?>" alt="<?php echo($service['name']); ?> Logo">
										<h4><?php echo($service['name']); ?></h4>
										<p><?php echo($service['description']); ?></p>
									</a>
								</div>
								<?php } ?>
							</div>
						</div>
						<?php } ?>
					
					</div>
					
					<!-- <div class="col-md-1"></div> -->
					
					<!-- sidebar -->
					<div class="col-md-4">
						<div class="services-sidebar ">
							<div class="services-area resources">
								<h4>Other Resources</h4>
								<ul>
									<li><a href="#">Resource 1</a></li>
									<li><a href="#">Resource 2</a></li>
									<li><a href="#">Resource 3</a></li>
									<li><a href="#">Resource 4</a></li>
									<li><a href="#">Resource 5</a></li>
									<li><a href="#">Resource 6</a></li>
									<li><a href="#">Resource 7</a></li>
								</ul>
							</div>
							<div class="services-area resources">
								<h4>Safari Books Bibliography</h4>
								<ul>
									<li><a href="#">Safari Book 1</a></li>
									<li><a href="#">Safari Book 2</a></li>
									<li><a href="#">Safari Book 3</a></li>
									<li><a href="#">Safari Book 4</a></li>
									<li><a href="#">Safari Book 5</a></li>
								</ul>
							</div>
						</div>
					</div><!-- sidebar -->
				
				</div>
				
			</div><!-- services -->