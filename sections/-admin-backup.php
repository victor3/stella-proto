			<div class="container">
				<div class="breadcrumbs"><a href="./">Home</a> /</div>
				<!-- DASHBOARD -->
				<h1 class="headline">Admin</h1>
				
				<ul id="admin-sections-menu" class="nav nav-tabs">
					<li id="users-menu" role="presentation" class="active"><a href="javascript:show_section('users','admin-sections');">Users</a></li>
					<li id="resources-menu" role="presentation"><a href="javascript:show_section('resources','admin-sections');">Resources</a></li>
					<li id="courses-menu" role="presentation"><a href="javascript:show_section('courses','admin-sections');">Courses</a></li>
					<li id="experiences-menu" role="presentation"><a href="javascript:show_section('experiences','admin-sections');">Experiences</a></li>
					<li id="tracks-menu" role="presentation"><a href="javascript:show_section('tracks','admin-sections');">Tracks</a></li>
					<li id="cohorts-menu" role="presentation"><a href="javascript:show_section('cohorts','admin-sections');">Cohorts</a></li>
				</ul>
				
				<div id="admin-sections">
					
					<!-- users -->
					<div id="users-box" class="section">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Users <a class="btn btn-bottom pull-right" href="javascript:edit('user','');">Add User</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th width="10%">Username</th>
										<th width="15%">Name</th>
										<th width="10%">Email</th>
										<th width="20%">Cohort</th>
										<th width="20%">Tracks</th>
										<!-- <th width="20%">Actions</th> -->
									</tr>
								</thead>
								<tbody>
									<?php for($i=0; $i<14; $i++) { ?>
									<tr>
										<!-- <th><input type="checkbox" id="user-<?php echo($i); ?>"></th> -->
										<td><?php echo($i); ?></td>
										<td><a href="#">Username</a></td>
										<td>Firstname Lastname</td>
										<td><a href="#">username@email.com</a></td>
										<td>
											<select class="form-control">
												<option name="CohortID" value="cohort-1">Cohort 1</option>
												<option name="CohortID" value="cohort-2">Cohort 2</option>
											</select>
										</td>
										<td>
											<label class="checkbox-inline">
											  <input type="checkbox" id="Track1" value="track-1"> Track 1
											</label>
											<label class="checkbox-inline">
											  <input type="checkbox" id="Track2" value="track-2"> Track 2
											</label>
											<label class="checkbox-inline">
											  <input type="checkbox" id="Track3" value="track-3"> Track 3
											</label>
										</td>
										<!-- <td>
											<a class="btn btn-wire" href="javascript:edit('user','user-<?php echo($i); ?>');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td> -->
									</tr>	
									<?php } ?>
								</tbody>
							</table>
						</div>
						<nav class="center-block clearfix">
							<ul class="pagination">
								<li>
									<a href="#" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
									</a>
								</li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li>
									<a href="#" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							</ul>
						</nav>
					</div>

					
					<!-- resources -->
					<div id="resources-box" class="section admin-section hidden">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Resources <a class="btn btn-bottom pull-right" href="javascript:edit('resource','');">Add Resource</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th class="opt">Description</th>
										<th>URL</th>
										<th class="opt">Icon (URL)</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php
									global $db;
									$i = 0;
									foreach ($db['services'] as $svc) {
										$i++;
										?>
										<tr>
											<!-- <th><input type="checkbox" id="experience-<?php echo($svc['id']); ?>"></th> -->
											<td><strong><?php echo($i); ?></strong></td>
											<td><a href="<?php echo($svc['url']); ?>"><?php echo($svc['name']); ?></a></td>
											<td><?php echo($svc['description']); ?></td>
											<td><a href="<?php echo($svc['url']); ?>"><?php echo($svc['url']); ?><a href="#"></td>
											<td><img class="service-icon img-responsive" src="<?php echo($svc['icon']); ?>" alt="<?php echo($svc['name']); ?> Icon"></td>
											<td>
												<a class="btn btn-wire" href="javascript:edit('resource','<?php echo($exp['id']); ?>');">Edit</a>
												<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>

					
					<!-- courses -->
					<div id="courses-box" class="section admin-section hidden">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Courses <a class="btn btn-bottom pull-right" href="javascript:edit('course','');">Add Course</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th>URL</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php
									global $db;
									$i = 0;
									foreach ($db['courses'] as $course) {
										$i++;
										?>
										<tr>
											<!-- <th><input type="checkbox" id="course-<?php echo($i); ?>"></th> -->
											<td><strong><?php echo($i); ?></strong></td>
											<td><a href="#"><?php echo($course['name']); ?></a></td>
											<td><?php echo($course['url']); ?></td>
											<td>
												<a class="btn btn-wire" href="javascript:edit('course','course-<?php echo($course['id']); ?>');">Edit</a>
												<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>

					
					<!-- experiences -->
					<div id="experiences-box" class="section admin-section hidden">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Experiences <a class="btn btn-bottom pull-right" href="javascript:edit('experience','');">Add Experience</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th>Courses</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php
									global $db;
									$i = 0;
									foreach ($db['experiences'] as $exp) {
										$i++;
										?>
										<tr>
											<!-- <th><input type="checkbox" id="experience-<?php echo($exp['id']); ?>"></th> -->
											<td><strong><?php echo($i); ?></strong></td>
											<td><a href="#"><?php echo($exp['name']); ?></a></td>
											<td><?php echo(count($exp['courses']).' courses'); ?></td>
											<td>
												<a class="btn btn-wire" href="javascript:edit('experience','<?php echo($exp['id']); ?>');">Edit</a>
												<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>

					
					<!-- tracks -->
					<div id="tracks-box" class="section admin-section hidden">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Tracks <a class="btn btn-bottom pull-right" href="javascript:edit('track','');">Add Track</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th>Description</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<!-- <th><input type="checkbox" id="track-1"></th> -->
										<td>1</td>
										<td><a href="#">Track 0</a></td>
										<td>Foundation Track</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('track','track-1');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
									<tr>
										<!-- <th><input type="checkbox" id="track-2"></th> -->
										<td>2</td>
										<td><a href="#">Track 1</a></td>
										<td>IoE Data Analyst</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('track','track-2');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
									<tr>
										<!-- <th><input type="checkbox" id="track-3"></th> -->
										<td>3</td>
										<td><a href="#">Track 2</a></td>
										<td>IoE Field Engineer</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('track','track-3');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					
					<!-- cohorts -->
					<div id="cohorts-box" class="section admin-section hidden">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Cohorts <a class="btn btn-bottom pull-right" href="javascript:edit('cohort','');">Add Cohort</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th>Place</th>
										<th>Tracks</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<!-- <th><input type="checkbox" id="cohort-1"></th> -->
										<td>1</td>
										<td><a href="#">Cohort 1</a></td>
										<td>San Francisco</td>
										<td>
											<div class="track">Track 1 (Default)</div>
											<div class="track">Track 2</div>
											<div class="track">Track 3</div>
										</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('cohort','cohort-1');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
									<tr>
										<!-- <th><input type="checkbox" id="cohort-2"></th> -->
										<td>2</td>
										<td><a href="#">Cohort 2</a></td>
										<td>Berlin</td>
										<td>
											<div class="track">Track 4 (Default)</div>
											<div class="track">Track 5 (Default)</div>
											<div class="track">Track 6</div>
										</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('cohort','cohort-2');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				
				</div>
				
			</div><!-- .container -->
			
			
			<!-- edit modal -->
			<div id="edit-modal" class="container-fluid">
				
				<!-- <a class="btn btn-space btn-wire pull-right" href="javascript:edit('close');">Close</a> -->
				
				<!-- container -->
				<div class="form-container">
					
					
					<!-- user -->
					<div id="edit-user-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save User</button>
							</div>
						
							<h3><span></span> User</h3>
						
							<div class="form-group">
								<label for="UserUsername">Username</label>
								<input id="UserUsername" class="form-control" placeholder="Enter username" type="text" />
							</div>
							<div class="form-group">
								<label for="UserName">Name</label>
								<input id="UserName" class="form-control" placeholder="Enter Full Name" type="text" />
							</div>
							<div class="form-group">
								<label for="UserEmail">Email</label>
								<input id="UserEmail" class="form-control" placeholder="Enter Email" type="text" />
							</div>
						</div>
					</div>
					
					<!-- resource -->
					<div id="edit-resource-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Resource</button>
							</div>
						
							<h3><span></span> Resource</h3>
						
							<div class="form-group">
								<label for="ResourceName">Name</label>
								<input id="ResourceName" class="form-control" placeholder="Enter Resource Name" type="text" />
							</div>
							<div class="form-group">
								<label for="ResourceDescription">Description (optional)</label>
								<textarea id="ResourceDescription" class="form-control" placeholder="Enter Resource Description" rows="3"></textarea>
							</div>
							<div class="form-group">
								<label>
									<input type="checkbox" id="CiscoResource" /> Cisco Resource
								</label>
							</div>
							<div class="form-group">
								<label for="ResourceURL">URL</label>
								<input id="ResourceURL" class="form-control" placeholder="Enter Resource URL" type="text" />
							</div>
							<div class="form-group">
								<label for="ResourceIconURL">Icon (URL)</label>
								<input id="ResourceIconURL" class="form-control" placeholder="Enter Icon URL" type="text" />
							</div>
						</div>
					</div>
					
					
					<!-- course -->
					<div id="edit-course-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Course</button>
							</div>
						
							<h3><span></span> Course</h3>
							
							<div class="form-group">
								<label for="CourseName">Course Name</label>
								<input id="CourseName" class="form-control" placeholder="Enter Course Name" type="text" />
							</div>
							<div class="form-group">
								<label for="CourseDescription">Course Description</label>
								<input id="CourseDescription" class="form-control" placeholder="Enter Course Description" type="text" />
							</div>
							<div class="form-group">
								<label for="CourseURL">Course URL</label>
								<input id="CourseURL" class="form-control" placeholder="Enter Course URL" type="text" />
							</div>
						</div>
					</div>
					
					
					<!-- experience -->
					<div id="edit-experience-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Experience</button>
							</div>
							
							<h3><span></span> Experience</h3>
						
							<div class="form-group">
								<label for="ExperienceName">Name</label>
								<input id="ExperienceName" class="form-control" placeholder="Enter Experience Name" type="text" />
							</div>
							<div class="form-group">
								<label for="ExperienceDescription">Description</label>
								<textarea id="ExperienceDescription" class="form-control" placeholder="Enter Experience Description" rows="3"></textarea>
							</div>
							<div class="form-group">
								<label for="ExperienceDate">Date <small>(Optional)</small></label>
								<input id="ExperienceDate" class="form-control" placeholder="Enter Experience Date" type="text" />
							</div>
							<div class="form-group">
								<label for="ExperienceHours">Hours <small>(Optional)</small></label>
								<input id="ExperienceHours" class="form-control" placeholder="Enter Experience Hours" type="text" />
							</div>
							<div class="form-group">
								<label class="wide">Courses</label>
								<div class="row narrow">
								<?php
								global $db;
								$i = 0;
								foreach ($db['courses'] as $course) {
									$i++;
									?>
									<div class="form-box fixed-height col-md-3 col-sm-4">
										<label>
											<input type="checkbox" id="course-<?php echo($i); ?>" /> <?php echo($course['name']); ?>
										</label>
									</div>
									<?php
								}
								?>
								</div>
							</div>
							<div class="form-group">
								<label class="wide">Header Image</label>
								<div class="row narrow">
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage1" />
											Header Image 1
										</label>
									</div>
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage2" />
											Header Image 2
										</label>
									</div>
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="cHeaderImage3" />
											Header Image 3
										</label>
									</div>
									<hr class="visible-sm-block">
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage4" />
											Header Image 4
										</label>
									</div>
									<hr class="visible-md-block">
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage5" />
											Header Image 5
										</label>
									</div>
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage6" />
											Header Image 6
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					<!-- track -->
					<div id="edit-track-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Track</button>
							</div>
						
							<h3><span></span> Track</h3>
						
							<div class="form-group">
								<label for="TrackName">Track Name</label>
								<input id="TrackName" class="form-control" placeholder="Enter Track Name" type="text" />
							</div>
							<div class="form-group">
								<label for="TrackDescription">Track Description <small>(Optional)</small></label>
								<input id="TrackDescription" class="form-control" placeholder="Enter Track Description" type="text" />
							</div>
							<div class="form-group">
								<label class="wide">Experiences</label>
								<div class="row narrow">
								<?php
								global $db;
								foreach ($db['experiences'] as $exp) {
									?>
									<div class="form-box fixed-height col-md-4 col-sm-6">
										<label>
											<input type="checkbox" id="experience-<?php echo($exp['id']); ?>" /> <?php echo($exp['name']); ?>
										</label>
									</div>
									<?php
								}
								?>
								</div>
							</div>
						</div>
					</div>
					
					
					<!-- cohort -->
					<div id="edit-cohort-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Cohort</button>
							</div>
						
							<h3><span></span> Cohort</h3>
							
							<div class="form-group">
								<label for="CohortName">Cohort Name</label>
								<input id="CohortName" class="form-control" placeholder="Enter Cohort Name" type="text" />
							</div>
							<div class="form-group">
								<label for="CohortDescription">Cohort Description <small>(Optional)</small></label>
								<input id="CohortDescription" class="form-control" placeholder="Enter Cohort Description" type="text" />
							</div>
							<div class="form-group">
								<label class="wide">Tracks</label>
								<div class="row narrow">
									<div class="form-box col-md-4 col-sm-6">
										<label>
											<input type="checkbox" id="track-1" class="track-selector" /> Track 1
										</label>
										<small class="pull-right">(<input type="checkbox" id="track-1-default" /> Make default)</small>
									</div>
									<div class="form-box col-md-4 col-sm-6">
										<label>
											<input type="checkbox" id="track-2" class="track-selector" /> Track 2
										</label>
										<small class="pull-right">(<input type="checkbox" id="track-1-default" /> Make default)</small>
									</div>
									<div class="form-box col-md-4 col-sm-6">
										<label>
											<input type="checkbox" id="track-3" class="track-selector" /> Track 3
										</label>
										<small class="pull-right">(<input type="checkbox" id="track-1-default" /> Make default)</small>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
					
				</div><!-- container -->
				
				<!-- bg -->
				<div class="bg"></div>
				
			</div><!-- edit modal -->

