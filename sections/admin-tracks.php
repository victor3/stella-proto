			<div class="container">
				
				<div id="admin-sections">
					
					<!-- tracks -->
					<div id="tracks-box" class="section admin-section">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Tracks <a class="btn btn-bottom pull-right" href="javascript:edit('track','');">Add Track</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th>Description</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<!-- <th><input type="checkbox" id="track-1"></th> -->
										<td>1</td>
										<td><a href="#">Track 0</a></td>
										<td>Foundation Track</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('track','track-1');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
									<tr>
										<!-- <th><input type="checkbox" id="track-2"></th> -->
										<td>2</td>
										<td><a href="#">Track 1</a></td>
										<td>IoE Data Analyst</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('track','track-2');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
									<tr>
										<!-- <th><input type="checkbox" id="track-3"></th> -->
										<td>3</td>
										<td><a href="#">Track 2</a></td>
										<td>IoE Field Engineer</td>
										<td>
											<a class="btn btn-wire" href="javascript:edit('track','track-3');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				
				</div>
				
			</div><!-- .container -->
			
			
			<!-- edit modal -->
			<div id="edit-modal" class="container-fluid">
				
				<!-- <a class="btn btn-space btn-wire pull-right" href="javascript:edit('close');">Close</a> -->
				
				<!-- container -->
				<div class="form-container">
					
					
					<!-- track -->
					<div id="edit-track-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Track</button>
							</div>
						
							<h3><span></span> Track</h3>
						
							<div class="form-group">
								<label for="TrackName">Track Name</label>
								<input id="TrackName" class="form-control" placeholder="Enter Track Name" type="text" />
							</div>
							<div class="form-group">
								<label for="TrackDescription">Track Description <small>(Optional)</small></label>
								<input id="TrackDescription" class="form-control" placeholder="Enter Track Description" type="text" />
							</div>
							<div class="form-group">
								<label class="wide">Experiences</label>
								<div class="row narrow">
								<?php
								global $db;
								foreach ($db['experiences'] as $exp) {
									?>
									<div class="form-box fixed-height col-md-4 col-sm-6">
										<label>
											<input type="checkbox" id="experience-<?php echo($exp['id']); ?>" /> <?php echo($exp['name']); ?>
										</label>
									</div>
									<?php
								}
								?>
								</div>
							</div>
						</div>
					</div>
					
				</div><!-- container -->
				
				<!-- bg -->
				<div class="bg"></div>
				
			</div><!-- edit modal -->

