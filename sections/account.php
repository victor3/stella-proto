			<?php
				
				global $db;
				
			?>
						
			<div class="container">
				<div class="breadcrumbs">
					Stella /
				</div>
				
				<!-- Dashboard -->
				<h1 class="headline">Your Account</h1>
				
			</div>
			
			<div class="account-masthead">
				<div class="container-fluid">
					<div class="container">
						<div class="row">
							<div class="col-md-5">
								<h2 class="title"><img class="img-avatar-big" src="lib/img/?40x40;avatar" alt="">Victor Zambrano</h2>
							</div>
							<div class="col-md-7">
								<ul class="scoreboard pull-right">
									<li id="scoreboard-posts" class="scorebox">
										<div class="score-number">2</div>
										<div class="score-label">posts</div>
									</li>
									<li id="scoreboard-projects" class="scorebox">
										<div class="score-number">1</div>
										<div class="score-label">projects</div>
									</li>
									<li id="scoreboard-experiences" class="scorebox">
										<div class="score-number">12</div>
										<div class="score-label">experiences</div>
									</li>
									<li id="scoreboard-courses" class="scorebox">
										<div class="score-number">23</div>
										<div class="score-label">courses</div>
									</li>
									<li id="scoreboard-commits" class="scorebox">
										<div class="score-number">34</div>
										<div class="score-label">commits</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="account-content">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<h3>Account Settings</h3>
							<div class="account-settings">
								<div class="form-group">
									<label for="AccountUserame">Username</label>
									<div class="row">
										<div class="col-md-10">
											<input type="text" id="AccountUserame" class="form-control" placeholder="Enter username">
										</div>
										<div class="col-md-2">
											<button id="AccountUserame-edit" onclick="javascript:edit('AccountUserame');" class="button btn btn-wire">Edit</button>
											<button id="AccountUserame-save" onclick="javascript:save('AccountUserame');" class="button btn btn-primary hidden">Save</button>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="AccountPassword">Password</label>
									<div class="row">
										<div class="col-md-10">
											<input type="text" id="AccountPassword" class="form-control" placeholder="Enter username">
										</div>
										<div class="col-md-2">
											<button id="AccountPassword-edit" onclick="javascript:edit('AccountPassword');" class="button btn btn-wire">Edit</button>
											<button id="AccountPassword-save" onclick="javascript:save('AccountPassword');" class="button btn btn-primary hidden">Save</button>
										</div>
									</div>
								</div>
								<!-- <div class="form-group">
									<label for="AccountUserame">Username</label>
									<div class="row">
										<div class="col-md-10">
											<input type="text" id="AccountUserame" class="form-control" placeholder="Enter username">
										</div>
										<div class="col-md-2">
											<button id="AccountUserame-edit" onclick="javascript:edit('AccountUserame');" class="button btn btn-wire">Edit</button>
											<button id="AccountUserame-save" onclick="javascript:save('AccountUserame');" class="button btn btn-primary hidden">Save</button>
										</div>
									</div>
								</div> -->
							</div>
						</div>
						
						<div class="col-md-2"></div>
					
						<div class="col-md-6">
							<div class="show-data">
								<div class="show-section">
									<h3>Recommended Jobs</h3>
									<div class="show-jobs">
										
									</div>
								</div>
								<div class="show-section">
									<h3>Recommended Jobs</h3>
									<div class="show-jobs">
										
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
			