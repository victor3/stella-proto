			<div class="container">
				
				<div id="admin-sections">
					
					<!-- experiences -->
					<div id="experiences-box" class="section admin-section">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Experiences <a class="btn btn-bottom pull-right" href="javascript:edit('experience','');">Add Experience</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th>Name</th>
										<th>Courses</th>
										<th width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php
									global $db;
									$i = 0;
									foreach ($db['experiences'] as $exp) {
										$i++;
										?>
										<tr>
											<!-- <th><input type="checkbox" id="experience-<?php echo($exp['id']); ?>"></th> -->
											<td><strong><?php echo($i); ?></strong></td>
											<td><a href="#"><?php echo($exp['name']); ?></a></td>
											<td><?php echo(count($exp['courses']).' courses'); ?></td>
											<td>
												<a class="btn btn-wire" href="javascript:edit('experience','<?php echo($exp['id']); ?>');">Edit</a>
												<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				
				</div>
				
			</div><!-- .container -->
			
			
			<!-- edit modal -->
			<div id="edit-modal" class="container-fluid">
				
				<!-- <a class="btn btn-space btn-wire pull-right" href="javascript:edit('close');">Close</a> -->
				
				<!-- container -->
				<div class="form-container">
					
					<!-- experience -->
					<div id="edit-experience-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save Experience</button>
							</div>
							
							<h3><span></span> Experience</h3>
						
							<div class="form-group">
								<label for="ExperienceName">Name</label>
								<input id="ExperienceName" class="form-control" placeholder="Enter Experience Name" type="text" />
							</div>
							<div class="form-group">
								<label for="ExperienceDescription">Description</label>
								<textarea id="ExperienceDescription" class="form-control" placeholder="Enter Experience Description" rows="3"></textarea>
							</div>
							<div class="form-group">
								<label for="ExperienceDate">Date <small>(Optional)</small></label>
								<input id="ExperienceDate" class="form-control" placeholder="Enter Experience Date" type="text" />
							</div>
							<div class="form-group">
								<label for="ExperienceHours">Hours <small>(Optional)</small></label>
								<input id="ExperienceHours" class="form-control" placeholder="Enter Experience Hours" type="text" />
							</div>
							<div class="form-group">
								<label class="wide">Courses</label>
								<div class="row narrow">
								<?php
								global $db;
								$i = 0;
								foreach ($db['courses'] as $course) {
									$i++;
									?>
									<div class="form-box fixed-height col-md-3 col-sm-4">
										<label>
											<input type="checkbox" id="course-<?php echo($i); ?>" /> <?php echo($course['name']); ?>
										</label>
									</div>
									<?php
								}
								?>
								</div>
							</div>
							<div class="form-group">
								<label class="wide">Header Image</label>
								<div class="row narrow">
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage1" />
											Header Image 1
										</label>
									</div>
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage2" />
											Header Image 2
										</label>
									</div>
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="cHeaderImage3" />
											Header Image 3
										</label>
									</div>
									<hr class="visible-sm-block">
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage4" />
											Header Image 4
										</label>
									</div>
									<hr class="visible-md-block">
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage5" />
											Header Image 5
										</label>
									</div>
									<div class="form-box col-md-3 col-sm-4">
										<label>
											<img class="img-responsive img-bottom" src="lib/img/?400x100" alt="" />
											<input type="radio" name="HeaderImages" id="HeaderImage6" />
											Header Image 6
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div><!-- container -->
				
				<!-- bg -->
				<div class="bg"></div>
				
			</div><!-- edit modal -->

