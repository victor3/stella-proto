			<?php
				$name = (isset($_GET['name'])) ? $_GET['name'] : '[Category Name]';
			?>

			<div class="container">
				<div class="breadcrumbs">
					Stella / 
					<a href="./?id=discussions">Discussions</a> /
				</div>
				
				<!-- CATEGORIES -->
				<h1 class="headline"><?php echo(urldecode($name)); ?></h1>
				<button class="btn btn-bottom btn-large pull-right" href="#">Start a Thread</button>
				<p class="description">Threads:</p>
				
				<div class="spaces row">
					<div class="col-xs-12">
						<?php //discussions_cat($name,5);
						global $db;
						$n = 9999;
						$n = ($n>count($db['threads'])) ? count($db['threads']) : $n;
						for ($i=0; $i<$n; $i++) {
							$thread = $db['threads'][$i];
							?>
							<div id="discussions-item-<?php echo($i); ?>" class="discussions-item <?php if (($i+1)==$n) echo('border-bottom'); ?>">
								<div class="row">
									<div class="discussions-title col-md-5">
										<h3><a href="./?id=discussions&section=thread&name=<?php echo(str_replace(' ','+',$thread['name'])); ?>&parent=<?php echo(str_replace(' ','+',$name)); ?>"><?php echo($thread['name']); ?></a></h3>
										<p class="discussions-meta">Started by <a href="#"><?php echo($thread['started_by']); ?></a> <?php echo(pretty($thread['time_created'])); ?></p>
									</div>
									<div class="discussions-data col-md-4">
										<p class="discussions-meta"><?php echo($thread['replies']); ?> Replies</p>
										<p class="discussions-comment"><a href="#"><?php echo($thread['recent_post']['username']); ?></a> <?php echo($thread['recent_post']['content']); ?> <small><?php echo(pretty($thread['recent_post']['timestamp'])); ?></small></p>
									</div>
									<div class="col-md-1"></div>
									<div class="discussions-people col-md-3">
										<!-- <span class="discussions-people-label col-xs-12">Participants</span> -->
										<p class="discussions-meta"><?php echo($thread['members']); ?> Members</p>
										<ul class="list-unstyled list-inline">
										<?php $o=0; while($o++<intval($thread['members'])) : ?>
											<li><img class="avatar-small" src="lib/img/?avatar"></li>
										<?php endwhile; ?>
										</ul>
									</div>
								</div>
							</div>
						<?php
						}
						?>
					</div>
					
					<nav class="text-center">
						<ul class="pagination">
							<li>
								<a href="#" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
					</nav>
					
				</div>
				
			</div><!-- container -->