			<?php
			$section = (isset($_GET['section'])) ? $_GET['section'] : 'users';
			?>

			<div class="container">
				
				<div class="breadcrumbs"><a href="./">Home</a> /</div>
				<!-- DASHBOARD -->
				<h1 class="headline">Admin</h1>
				
				<ul id="admin-sections-menu" class="nav nav-tabs">
					<li id="users-menu" role="presentation" class="<?php if ($section=='users') echo('active'); ?>"><a href="./?id=admin&section=users">Users</a></li>
					<li id="resources-menu" role="presentation" class="<?php if ($section=='resources') echo('active'); ?>"><a href="./?id=admin&section=resources">Resources</a></li>
					<li id="courses-menu" role="presentation" class="<?php if ($section=='courses') echo('active'); ?>"><a href="./?id=admin&section=courses">Courses</a></li>
					<li id="experiences-menu" role="presentation" class="<?php if ($section=='experiences') echo('active'); ?>"><a href="./?id=admin&section=experiences">Experiences</a></li>
					<li id="tracks-menu" role="presentation" class="<?php if ($section=='tracks') echo('active'); ?>"><a href="./?id=admin&section=tracks">Tracks</a></li>
					<li id="cohorts-menu" role="presentation" class="<?php if ($section=='cohorts') echo('active'); ?>"><a href="./?id=admin&section=cohorts">Cohorts</a></li>
				</ul>
				
			</div>
			
			<?php include "./sections/admin-".$section.".php"; ?>