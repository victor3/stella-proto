			<?php
				global $db;
				global $n;
				$n = 9999;
				$n = ($n>count($db['experiences'])) ? count($db['experiences']) : $n;
			?>
			
			<div class="container">
				<div class="breadcrumbs">
					Stella /
				</div>				

				<h1 class="headline">Experiences</h1>
			</div>
					
			<!-- dashboard carousel -->
			<div id="carousel-container" class="container-fluid">						
				<div id="progress-track" class="progress track">
				<?php 
					global $db;
					$total_hours = 0;
					for ($i=0; $i<(count($db['experiences'])); $i++) {
						$total_hours += intval($db['experiences'][$i]['hour_length']);
					}
					for ($i=0; $i<(count($db['experiences'])); $i++) {
						$exp = $db['experiences'][$i];
						$hours = intval($exp['hour_length']);
						$per = round( ((100*$hours)/$total_hours), 4, PHP_ROUND_HALF_DOWN);
						$total_per += $per;
					?>
					<style>
						#exp-bar-<?php echo($exp['id']); ?> {
							/*color: <?php echo($exp['image_ref']); ?> !important;
							background-color: <?php echo($exp['image_ref']); ?> !important;*/
							width: <?php echo($per); ?>%;
						}
					</style>
					<a href="javascript:showexp('<?php echo($exp['id']); ?>');" id="exp-bar-<?php echo($exp['id']); ?>" class="progress-bar learning-online" data-toggle="popover" data-placement="top" title="<?php echo($exp['name']); ?>" data-content="<?php echo($exp['description']); ?>"><?php echo($exp['hour_length']); ?> h</a>
				<?php } ?>
				</div>
				
				<!-- carousel -->
				<div id="carousel-container-box" class="carousel-box toggle-box">
					<!--
					// CAROUSEL ////////////////////////////////////////////////	
					-->
					<?php experience_carousel_new(); ?>
					
					<a class="prev-slide">
						<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
					</a>
					<a class="next-slide">
						<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
					</a>
					
					<?php //experience_carousel('xtrasmall'); ?>
					
					<?php //experience_carousel('small'); ?>
					
					<?php //experience_carousel('medium'); ?>
					
					<?php //experience_carousel('large'); ?>
					<!--
					// CAROUSEL ////////////////////////////////////////////////	
					-->
						
					<a id="carousel-container-button-on" class="pull-right carousel-tab-down carousel-container-button-on" href="javascript:toggle_class('carousel-container');exp_header();">Collapse Experiences</a>
					
				</div>
				
					
		
			</div><!-- container-fluid -->
				
				
				<div class="container-fluid">

					<!-- EXPERIENCE PAGES -->
					<div id="show-experience-page">
						<!-- <div class="container"> -->
						<?php // experiences pages
						for ($k=0; $k<$n; $k++) {
							global $db;
							$experience = $db['experiences'][$k];
						?>
				
						<style>
							#experience-wrapper-<?php echo($experience['id']); ?> .course-box,
							#experience-wrapper-<?php echo($experience['id']); ?> .project-box {
								border-top: 6px solid <?php echo($experience['image_ref']); ?> !important;
							}
						</style>
		
						<!-- experience page -->
						<div id="experience-wrapper-<?php echo($experience['id']); ?>" class="experience-wrapper">
							<div class="experience-page">
						
								<!-- experience masthead -->
								<div id="experience-masthead-<?php echo($experience['id']); ?>" class="experience-masthead">
									<div class="container">
										<a id="carousel-container-button-off" class="btn btn-top btn-wire toggle-button hidden-item pull-right carousel-container-button-off" href="javascript:toggle_class('carousel-container');exp_header('<?php echo($experience['id']); ?>');">Expand Carousel</a>
										<!-- <small>Experience</small> -->
										<h2><?php echo($experience['name']); ?></h2>
									</div>
								</div>
						
								<div class="container">
									<div class="row">
										<div class="col-md-6">
											<div class="experience-overview-menu">
												<a id="experience-overview-<?php echo($experience['id']); ?>-button-on" class="overview-button-on hidden-item toggle-button" href="javascript:toggle_class('experience-overview-<?php echo($experience['id']); ?>')">Overview</a>
												<a id="experience-overview-<?php echo($experience['id']); ?>-button-off" class="overview-button-off pull-right" href="javascript:toggle_class('experience-overview-<?php echo($experience['id']); ?>');">Hide Overview</a>
											</div>
											<div id="experience-overview-<?php echo($experience['id']); ?>-box" class="experience-overview toggle-box">
												<p><?php 
													//echo($experience['description']);
													echo(txt(rand(500,1500)));
												?></p>
											</div>
											<div class="activity-stream">
												<h3>Notifications</h3>
												<div class="note-posts">
													<div class="form-group">
														<textarea id="note-textarea-<?php echo($experience['id']); ?>" class="reply-field form-control" rows="3"></textarea>
													</div>
													<button onclick="javascript:add_note('<?php echo($experience['id']); ?>');" class="reply-submit-button btn btn-button btn-large">Post Notification</button>
												</div>
												<div id="notes-<?php echo($experience['id']); ?>"></div>
											</div>
										</div><!-- col-md-6 -->

										<div class="col-md-3 col-sm-6">
											<h3>Courses</h3>
											<?php
											foreach($experience['courses'] as $course_id) {
												$course = $db['courses'][$course_id];
												?>
												<div class="course-box">
													<h4><a href="<?php echo($course['url']); ?>" target="course"><?php echo($course['name']); ?></a></h4>
													<p><?php echo($course['description']); ?></p>
												</div>
												<?php
											}
											?>
										</div><!-- col-md-3 -->

										<div class="col-md-3 col-sm-6">
											<a href="javascript:edit('project','');" class="btn btn-top btn-small pull-right">Add</a>
											<h3>Projects</h3>
											<?php
											$sponsored_projs = array();
											$student_projs = array();
											foreach($experience['projects'] as $project_id) {
												if ($db['projects'][$project_id]['sponsor']!='') {
													$sponsored_projs[] = $db['projects'][$project_id];
												} else {
													$student_projs[] = $db['projects'][$project_id];
												}
											}
											foreach($sponsored_projs as $proj) {
												?>
												<div class="project-box">
													<div class="sponsor" style="/*background-color:<?php echo($experience['image_ref']); ?>;*/">
														<a href="#"><span class="sponsor-name"><small>Sponsored by</small> <?php echo($proj['sponsor']['name']); ?></span> <span class="sponsor-icon"><img src="<?php echo($proj['sponsor']['icon']); ?>" alt="<?php echo($proj['sponsor']['name']); ?>"></span></a>
													</div>
													<h4><a href="./?id=project&project=<?php echo($proj['id']); ?>"><?php echo($proj['name']); ?></a></h4>
													<div class="project-team"><!-- Team: -->
													<?php foreach($proj['team'] as $team) { ?>
														<img class="img-avatar img-circle" src="lib/img/?32x32;avatar" alt="Team Member Avatar">
													<?php } ?>
													</div>
													<div class="project-desc">
														<p><?php echo($proj['description']); ?></p>
													</div>
												</div>
												<?php
											}
											foreach($student_projs as $proj) {
												?>
												<div class="project-box">
													<h4><a href="./?id=project&project=<?php echo($proj['id']); ?>"><?php echo($proj['name']); ?></a></h4>
													<div class="project-team"><!-- Team: -->
													<?php foreach($proj['team'] as $team) { ?>
														<img class="img-circle" src="lib/img/?32x32;avatar" alt="Team Member Avatar">
													<?php } ?>
													</div>
													<div class="project-desc">
														<p><?php echo($proj['description']); ?></p>
													</div>
								
												</div>
												<?php
											}
											?>
										</div><!-- col-md-3 -->
								
									</div><!-- row -->
								</div><!-- container -->
							</div><!-- .experience-page -->
						</div><!-- #experience-wrapper-id -->
		
						<?php } ?>
			
					</div><!-- show-experience-page -->
		
					<!-- show first experience -->
					<script>
						var type = 'xtrasmall';
						if ( $(window).width() > 438) {
							if ( $(window).width() > 760) {
								if ( $(window).width() > 1440) {
									type = 'large';
								}
								type = 'medium';
							}
							type = 'small';
						}
						showexp('<?php echo($db['experiences'][0]['id']); ?>',type);
					</script>
		
				</div><!-- .container-fluid -->
				
				<!-- edit modal -->
				<div id="edit-modal" class="container-fluid">
					<!-- container -->
					<div class="form-container">
						<!-- project -->
						<div id="edit-project-box" class="edit-box container">
						
							<div class="form">
							
								<div class="form-group pull-right">
									<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
									<button class="btn btn-default" onclick="javascript:edit('close');">Create Project</button>
								</div>
						
								<h3><span></span> Project</h3>
						
								<div class="form-group">
									<label for="ProjectName">Project Name</label>
									<input id="ProjectName" class="form-control" placeholder="Enter project name" type="text" />
								</div>
								<div class="form-group">
									<label for="ProjectOverview">Project Description</label>
									<textarea id="ProjectOverview" rows="3" class="form-control"></textarea>
								</div>

								<div class="form-group">
									<label for="team-member-field">Invite Team Member(s)</label>
									<div class="input-group">
										<span class="input-group-addon">@</span>
										<input id="team-member-field" class="form-control" placeholder="Enter Email" type="text" />
										<span class="input-group-btn">
											<button class="btn btn-default btn-group-left" type="button">Add Team Member</button>
										</span>
									</div>
								</div>
								<div class="form-group">
									<label for="team-member-container">Team Member(s)</label>
									<!-- <textarea id="team-member-textarea" rows="3" class="form-control"></textarea> -->
									<div id="team-member-container"></div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- bg -->
					<div class="bg"></div>
					
				</div>
					
			</div><!-- experiences -->



<?php

/* Functions
 *****************************************************************************/

function experience_carousel_new() {
	global $db;
	$experiences = $db['experiences'];
	?>
	<div id="carousel" class="owl-carousel">
		<?php foreach($experiences as $experience) { ?>
			<!-- experience item -->
			<div id="experience-item-<?php echo($experience['id']); ?>" class="experience-item item">
				<a class="experience-link" href="javascript:showexp('<?php echo($experience['id']); ?>');">
					<img class="experience-image img-responsive" src="<?php echo($experience['image_url']); ?>" alt="<?php echo($experience['name']); ?> Header Image" />
					<h4 class="experience-name"><?php echo($experience['name']); ?></h4>
					<!-- <p class="experience-meta"><strong>33% Completed</strong> / 12 Remaining Lessons</p> -->
					<p class="experience-desc"><?php echo($experience['description']); ?></p>
					<p class="experience-desc experience-hours"><?php echo($experience['hour_length']); ?> Hour<?php if (intval($experience['hour_length'])>1) echo "s"; ?></p>
					<button class="experience-button btn-button">Resume Experience</button>
				</a>
			</div><!-- dash-item -->
		<?php } ?>
	</div>
	<?php
}

function experience_carousel($type) {
	global $db;
	global $n;

	$num = 4;
	$col = 3;

	switch ($type) {
		case 'xtrasmall':
			$num = 1;
			$col = 12;
			break;
		case 'small':
			$num = 2;
			$col = 6;
			break;
		case 'medium':
			$num = 4;
			$col = 3;
			break;
		case 'large':
			$num = 6;
			$col = 2;
			break;
	}
?>

	<div id="carousel-example-generic-<?php echo($type); ?>" class="carousel carousel-<?php echo($type); ?> slide" data-interval="0" data-pause="hover"><!--  data-ride="carousel" -->

		<ol class="carousel-indicators">
		<?php for ($j=0; $j<$n; $j++) {
			$active = ($j==0) ? 'active' : '';
			if (($j%$num)==0) echo('<li data-target="#carousel-example-generic-'.$type.'" data-slide-to="'.$j.'" class="'.$active.'"></li>');
		} ?>
		</ol>

		<!-- carousel inner -->
		<div id="experience-container-<?php echo($type); ?>" class="experience-container carousel-inner row" role="listbox">
		<?php // dashboard item
			//$counter = 0;
			for ($i=0; $i<$n; $i++) {
				$experience = $db['experiences'][$i];
				$active = ($i==0) ? 'active' : '';
				$closed = ($i!=0) ? '</div><!-- end slide div -->' : '';
				if (($i%$num)==0) echo($closed.'<div class="item '.$active.'"><!-- start slide div -->');
				?>

				<!-- experience item -->
				<div id="experience-item-<?php echo($experience['id']); ?>-<?php echo($type); ?>" class="experience-item col-md-<?php echo($col); ?> col-sm-<?php echo($col); ?> col-xs-<?php echo($col); ?>">
					<a class="experience-link" href="javascript:showexp('<?php echo($experience['id']); ?>','<?php echo($type); ?>');">
						<img class="experience-image img-responsive" src="<?php echo($experience['image_url']); ?>" alt="<?php echo($experience['name']); ?> Header Image" />
						<h4 class="experience-name"><?php echo($experience['name']); ?></h4>
						<!-- <p class="experience-meta"><strong>33% Completed</strong> / 12 Remaining Lessons</p> -->
						<p class="experience-desc"><?php echo($experience['description']); ?></p>
						<p class="experience-desc experience-hours"><?php echo($experience['hour_length']); ?> Hour<?php if (intval($experience['hour_length'])>1) echo "s"; ?></p>
						<button class="experience-button btn-button">Resume Experience</button>
					</a>
				</div><!-- dash-item -->
		<?php } ?>
		</div><!-- experience container / carousel inner -->

		<!-- navigation -->
		<a class="left carousel-control" href="#carousel-example-generic-<?php echo($type); ?>" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic-<?php echo($type); ?>" role="button" data-slide="next">
			<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>

	</div>
</div>

<?php } ?>