				<div class="container">
					
					<!-- users -->
					<div id="users-box" class="section">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Users <a class="btn btn-bottom pull-right" href="javascript:edit('user','');">Add User</a></h3>
							</div>
							<!-- <div class="panel-body"></div> -->
							<table id="user-table" class="table table-striped">
								<thead>
									<tr>
										<!-- <th><a href="#">All</a></th> -->
										<th width="5%">#</th>
										<th width="10%">Username</th>
										<th width="15%">Name</th>
										<th width="10%">Email</th>
										<th width="20%">Cohort</th>
										<th width="20%">Tracks</th>
										<!-- <th width="20%">Actions</th> -->
									</tr>
								</thead>
								<tbody>
									<?php for($i=0; $i<14; $i++) { ?>
									<tr>
										<!-- <th><input type="checkbox" id="user-<?php echo($i); ?>"></th> -->
										<td><?php echo($i); ?></td>
										<td><a href="#">Username</a></td>
										<td>Firstname Lastname</td>
										<td><a href="#">username@email.com</a></td>
										<td>
											<select class="form-control">
												<option name="CohortID" value="cohort-1">Cohort 1</option>
												<option name="CohortID" value="cohort-2">Cohort 2</option>
											</select>
										</td>
										<td>
											<label class="checkbox-inline">
											  <input type="checkbox" id="Track1" value="track-1"> Track 1
											</label>
											<label class="checkbox-inline">
											  <input type="checkbox" id="Track2" value="track-2"> Track 2
											</label>
											<label class="checkbox-inline">
											  <input type="checkbox" id="Track3" value="track-3"> Track 3
											</label>
										</td>
										<!-- <td>
											<a class="btn btn-wire" href="javascript:edit('user','user-<?php echo($i); ?>');">Edit</a>
											<a class="btn btn-wire btn-delete" href="javascript:del();">Delete</a>
										</td> -->
									</tr>	
									<?php } ?>
								</tbody>
							</table>
						</div>
						<nav class="center-block clearfix">
							<ul class="pagination">
								<li>
									<a href="#" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
									</a>
								</li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li>
									<a href="#" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							</ul>
						</nav>
					</div>

				</div><!-- users-box -->
				
			</div><!-- .container -->
			
			
			<!-- edit modal -->
			<div id="edit-modal" class="container-fluid">
				
				<!-- <a class="btn btn-space btn-wire pull-right" href="javascript:edit('close');">Close</a> -->
				
				<!-- container -->
				<div class="form-container">
					
					
					<!-- user -->
					<div id="edit-user-box" class="edit-box container">
						
						<div class="form">
							
							<div class="form-group pull-right">
								<button class="btn btn-cancel" onclick="javascript:edit('close');">Cancel</button>
								<button class="btn btn-default" onclick="javascript:edit('close');">Save User</button>
							</div>
						
							<h3><span></span> User</h3>
						
							<div class="form-group">
								<label for="UserUsername">Username</label>
								<input id="UserUsername" class="form-control" placeholder="Enter username" type="text" />
							</div>
							<div class="form-group">
								<label for="UserName">Name</label>
								<input id="UserName" class="form-control" placeholder="Enter Full Name" type="text" />
							</div>
							<div class="form-group">
								<label for="UserEmail">Email</label>
								<input id="UserEmail" class="form-control" placeholder="Enter Email" type="text" />
							</div>
						</div>
					</div>
					
				</div><!-- form-container -->
				
				<!-- bg -->
				<div class="bg"></div>
				
			</div><!-- edit modal -->

