			<?php
			
				$tab = ($_GET['tab']=='services') ? 'services' : 'experiences';
				
				global $db;
				global $n;
				$n = 9999;
				$n = ($n>count($db['experiences'])) ? count($db['experiences']) : $n;
				
				// styles
				$e = $db['experiences'];
				
			?>
						
			<div class="container">
				<div class="breadcrumbs">
					Stella /
				</div>
				
				<!-- Dashboard -->
				<h1 class="headline">Dashboard</h1>
				<nav id="dashboard-sections-nav" role="navigation">
					<ul id="dashboard-sections-menu" class="nav nav-pills">
						<li id="experiences-menu" role="presentation" class="active"><a id="tab_experiences" href="javascript:show_section('experiences', 'dashboard-sections');">Experiences</a></li>
						<li id="services-menu" role="presentation"><a id="tab_services" href="javascript:show_section('services', 'dashboard-sections');">Services</a></li>
						<li id="my-account-menu" role="presentation" class="pull-right"><a id="tab_services" href="javascript:show_section('my-account', 'dashboard-sections');">Account</a></li>
					</ul>
				</nav>
				
			</div>
			
			<div id="dashboard-sections">
				
				<!-- experiences -->
				<div id="experiences-box" class="section">
					
					<!-- dashboard carousel -->
					<div id="carousel-container" class="container-fluid">						
						<div id="progress-track" class="progress track">
						<?php
							global $db;
							$total_hours = 0;
							for ($i=0; $i<(count($db['experiences'])); $i++) {
								$total_hours += intval($db['experiences'][$i]['hour_length']);
							}
							for ($i=0; $i<(count($db['experiences'])); $i++) {
								$exp = $db['experiences'][$i];
								$hours = intval($exp['hour_length']);
								$per = round( ((100*$hours)/$total_hours), 4, PHP_ROUND_HALF_DOWN);
								$total_per += $per;
							?>
							<style>
								#exp-bar-<?php echo($exp['id']); ?> {
									background-color: <?php echo($exp['image_ref']); ?>;
									width: <?php echo($per); ?>%;
								}
							</style>
							<div id="exp-bar-<?php echo($exp['id']); ?>" class="progress-bar learning-online">
								<div class="desc"><?php echo($exp['name']); ?></div>
							</div>
						<?php } ?>
						</div>
						
						<!-- carousel -->
						<div id="carousel-container-box" class="toggle-box">
							<!--
							// CAROUSEL ////////////////////////////////////////////////	
							-->
							<?php experience_carousel('small'); ?>
							
							<?php experience_carousel('medium'); ?>
							
							<?php experience_carousel('large'); ?>
							<!--
							// CAROUSEL ////////////////////////////////////////////////	
							-->
						</div>
						
							<div class="container">
								<a id="carousel-container-button-on" class="pull-right carousel-tab-down" href="javascript:toggle_class('carousel-container');exp_header();">Collapse Experiences</a>
							</div>
				
					</div><!-- container-fluid -->
				
				
					<div class="container-fluid">

						<!-- EXPERIENCE PAGES -->
						<div id="show-experience-page">
								
							<?php // experiences pages
							for ($k=0; $k<$n; $k++) {
								global $db;
								$experience = $db['experiences'][$k];
							?>
							
							<style>
								#experience-wrapper-<?php echo($experience['id']); ?> .course-box,
								#experience-wrapper-<?php echo($experience['id']); ?> .project-box {
									border-top: 6px solid <?php echo($experience['image_ref']); ?> !important;
								}
							</style>
					
							<!-- experience page -->
							<div id="experience-wrapper-<?php echo($experience['id']); ?>" class="experience-wrapper">
								<div class="experience-page">
									
									<!-- experience masthead -->
									<div id="experience-masthead-<?php echo($experience['id']); ?>" class="experience-masthead">
										<div class="container">
											<a id="carousel-container-button-off" class="btn btn-top btn-wire toggle-button hidden-item pull-right" href="javascript:toggle_class('carousel-container');exp_header();">Expand Carousel</a>
											<small>Experience</small>
											<h2><?php echo($experience['name']); ?></h2>
										</div>
									</div>
									
									<div class="container">
										<div class="row">
											<div class="col-md-6">
												<div class="experience-overview-menu">
													<a id="experience-overview-<?php echo($experience['id']); ?>-button-on" class="overview-button-on hidden-item toggle-button" href="javascript:toggle_class('experience-overview-<?php echo($experience['id']); ?>')">Overview</a>
													<a id="experience-overview-<?php echo($experience['id']); ?>-button-off" class="overview-button-off pull-right" href="javascript:toggle_class('experience-overview-<?php echo($experience['id']); ?>');">Hide Overview</a>
												</div>
												<div id="experience-overview-<?php echo($experience['id']); ?>-box" class="experience-overview toggle-box">
													<p><?php 
														//echo($experience['description']);
														echo(txt(rand(500,1500)));
													?></p>
												</div>
												<div class="activity-stream">
													<h3>Activity</h3>
													<?php
													$na = rand(1,count($db['activities'])-1);
													for ($l=$na; $l>0; $l--) {
														$activity = $db['activities'][$l];
														?>
														<div class="activity-item">
															<img class="img-circle" src="lib/img/?32x32;666;avatar" alt="icon">
															<div class="activity-content">
																<strong><a href="./?id=user&name=<?php echo($activity['user']); ?>"><?php echo($activity['user']); ?></a></strong> 
																<?php echo(txt(rand(40,140))); ?>
																<small><?php echo(pretty($activity['timestamp'])); ?></small>
																<br/>
																<a class="activity-action" href="#"><?php echo(ucfirst($activity['type'])); ?></a>
															</div>
														</div>
														<?php
													}
													?>
												</div>
											</div><!-- col-md-6 -->
			
											<div class="col-md-3">
												<h3>Courses</h3>
												<?php
												foreach($experience['courses'] as $course_id) {
													$course = $db['courses'][$course_id];
													?>
													<div class="course-box">
														<h4><a href="<?php echo($course['url']); ?>" target="course"><?php echo($course['name']); ?></a></h4>
														<p><?php echo($course['description']); ?></p>
													</div>
													<?php
												}
												?>
											</div><!-- col-md-3 -->
			
											<div class="col-md-3">
												<button class="btn btn-top btn-small pull-right">Add</button>
												<h3>Projects</h3>
												<?php
												foreach($experience['projects'] as $project_id) {
													$project = $db['projects'][$project_id];
													?>
													<div class="project-box">
														<h4><a href="./?id=project&project=<?php echo($project_id); ?>"><?php echo($project['name']); ?></a></h4>
														<div class="project-team"><!-- Team: -->
														<?php foreach($project['team'] as $team) { ?>
															<img class="img-circle" src="lib/img/?24x24;666;avatar" alt="Team Member Avatar">
														<?php } ?>
														</div>
														<div class="project-desc">
															<p><?php echo($project['description']); ?></p>
														</div>
											
													</div>
													<?php
												}
												?>
											</div><!-- col-md-3 -->
											
										</div><!-- container -->
			
									</div>
								</div><!-- .experience-page -->
							</div><!-- #experience-wrapper-id -->
					
							<?php } ?>
					
						</div>
				
				
						<!-- show first experience -->
						<script>
							var type = ( $(window).width() > 960 ) ? ( ( $(window).width() > 1440 ) ? 'large' : 'medium' ) : 'small'
							showexp('<?php echo($db['experiences'][0]['id']); ?>',type);
						</script>
				
					</div><!-- .container -->
					
				</div><!-- experiences -->
				
				
				
				<!-- services -->
				<div id="services-box" class="section">
					
					<div class="container">
						<div class="row">
						
							<!-- main area -->
							<div class="col-md-9">
								
								<div class="services-area">
									<h3>Cisco Services</h3>
									<div class="row narrow">
									<?php foreach($db['services']['cisco'] as $cisco) { ?>
										<div class="service-item col-md-3 col-sm-4 col-xs-6">
											<a href="<?php echo($cisco['url']); ?>">
												<img class="img-responsive" src="<?php echo(hex($cisco['icon'])); ?>" alt="<?php echo($cisco['name']); ?> Logo">
												<h4><?php echo($cisco['name']); ?></h4>
												<p><?php echo($cisco['description']); ?></p>
											</a>
										</div>
									<?php } ?>
									</div>
								</div>
								
								<div class="services-area">
									<h3>Services</h3>
									<div class="row narrow">
										<?php foreach($db['services']['other'] as $other) { ?>
											<div class="service-item col-md-3 col-sm-4 col-xs-6">
												<a href="<?php echo($other['url']); ?>">
													<img class="img-responsive" src="<?php echo(hex($other['icon'])); ?>" alt="<?php echo($other['name']); ?> Logo">
													<h4><?php echo($other['name']); ?></h4>
													<p><?php echo($other['description']); ?></p>
												</a>
											</div>
										<?php } ?>
									</div>
								</div>
							
							</div>
							
							<!-- sidebar -->
							<div class="col-md-3">
								<div class="services-area resources">
									<h4>Resources</h4>
									<ul>
										<li><a href="#">Resource 1</a></li>
										<li><a href="#">Resource 2</a></li>
										<li><a href="#">Resource 3</a></li>
										<li><a href="#">Resource 4</a></li>
										<li><a href="#">Resource 5</a></li>
										<li><a href="#">Resource 6</a></li>
										<li><a href="#">Resource 7</a></li>
										<li><a href="#">Resource 8</a></li>
										<li><a href="#">Resource 9</a></li>
										<li><a href="#">Resource 10</a></li>
										<li><a href="#">Resource 11</a></li>
									</ul>
								</div>
								<div class="services-area resources">
									<h4>References</h4>
									<ul>
										<li><a href="#">Reference 1</a></li>
										<li><a href="#">Reference 2</a></li>
										<li><a href="#">Reference 3</a></li>
										<li><a href="#">Reference 4</a></li>
										<li><a href="#">Reference 5</a></li>
										<li><a href="#">Reference 6</a></li>
										<li><a href="#">Reference 7</a></li>
										<li><a href="#">Reference 8</a></li>
										<li><a href="#">Reference 9</a></li>
									</ul>
								</div>
							</div><!-- sidebar -->
						
						</div>
					
					</div><!-- container -->
					
				</div><!-- services -->
				
				
				
				<!-- account -->
				<div id="my-account-box" class="section">
					
					<div class="container">
						
						<h3>Your Account</h3>
					
					</div>
					
				</div>
				
			</div><!-- dashboard sections -->
			
			

<?php

/* Functions
 *****************************************************************************/

function experience_carousel($type) {
	global $db;
	global $n;
	
	$num = 4;
	$col = 3;
	
	switch ($type) {
		case 'small':
			$num = 2;
			$col = 6;
			break;
		case 'medium':
			$num = 4;
			$col = 3;
			break;
		case 'large':
			$num = 6;
			$col = 2;
			break;
	}
?>
	
	<div id="carousel-example-generic-<?php echo($type); ?>" class="carousel carousel-<?php echo($type); ?> slide" data-interval="0" data-pause="hover"><!--	data-ride="carousel" -->
		
		<ol class="carousel-indicators">
		<?php for ($j=0; $j<$n; $j++) {
			$active = ($j==0) ? 'active' : '';
			if (($j%$num)==0) echo('<li data-target="#carousel-example-generic-'.$type.'" data-slide-to="'.$j.'" class="'.$active.'"></li>');
		} ?>
		</ol>
	
		<!-- carousel inner -->
		<div id="experience-container-<?php echo($type); ?>" class="experience-container carousel-inner row" role="listbox">
		<?php // dashboard item
			//$counter = 0;
			for ($i=0; $i<$n; $i++) {
				$experience = $db['experiences'][$i];
				$active = ($i==0) ? 'active' : '';
				$closed = ($i!=0) ? '</div><!-- end slide div -->' : '';
				if (($i%$num)==0) echo($closed.'<div class="item '.$active.'"><!-- start slide div -->');
				?>
			
				<!-- experience item -->
				<div id="experience-item-<?php echo($experience['id']); ?>-<?php echo($type); ?>" class="experience-item col-md-<?php echo($col); ?> col-sm-<?php echo($col); ?> col-xs-<?php echo($col); ?>">
					<a class="experience-link" href="javascript:showexp('<?php echo($experience['id']); ?>','<?php echo($type); ?>');">
						<img class="experience-image img-responsive" src="<?php echo($experience['image_url']); ?>" alt="<?php echo($experience['name']); ?> Header Image" />
						<h4 class="experience-name"><?php echo($experience['name']); ?></h4>
						<!-- <p class="experience-meta"><strong>33% Completed</strong> / 12 Remaining Lessons</p> -->
						<p class="experience-desc"><?php echo($experience['description']); ?></p>
						<p class="experience-desc experience-hours"><?php echo($experience['hour_length']); ?> Hour<?php if (intval($experience['hour_length'])>1) echo "s"; ?></p>
						<button class="experience-button btn-button">Resume Experience</button>
					</a>
				</div><!-- dash-item -->
		<?php } ?>
		</div><!-- experience container / carousel inner -->
		
		<!-- navigation -->
		<a class="left carousel-control" href="#carousel-example-generic-<?php echo($type); ?>" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic-<?php echo($type); ?>" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
		
	</div>
</div>

<?php } ?>