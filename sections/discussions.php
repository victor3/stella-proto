			<div class="container">
				<div class="breadcrumbs">
					Stella /
				</div>
				
				<!-- Campus -->
				<h1 class="headline">Discussions</h1>
				<p class="description">Categories:</p>
				
				<div class="spaces row">
					<div class="col-xs-12">
						<?php //discussions(6);
						global $db;
						$n = 9999;
						$n = ($n>count($db['discussions'])) ? count($db['discussions']) : $n;
						for ($i=0; $i<$n; $i++) {
							$discussions = $db['discussions'][$i];
							?>
							<div id="discussions-item-<?php echo($i); ?>" class="discussions-item <?php if (($i+1)==$n) echo('border-bottom'); ?>">
								<div class="row">
									<div class="discussions-header col-md-6">
										<h3><a href="./?id=discussions&section=category&name=<?php echo(safeurl($discussions['name'])); ?>"><?php echo($discussions['name']); ?></a></h3>
										<p class="discussions-meta-dark"><?php echo($discussions['discussions']); ?> Discussions / <?php echo($discussions['members']); ?> Members</p>
										<p class="discussions-meta"><?php echo(txt(rand(120,240))); ?></p>
									</div>
									<div class="discussions-data col-md-6">
										<p class="discussions-meta">Recent Discussions</p>
										<?php
										$threads = $db['threads'];
										for ($j=0; $j<(count($threads)-1); $j++) {
											$thread = $threads[$j];
											if (in_array($discussions['id'], $thread['parent_id'])) {
											?>
											<div class="discussions-comment">
												<h4><a href="./?id=discussions&section=thread&name=<?php echo(safeurl($thread['name'])); ?>&parent=<?php echo(safeurl($discussions['name'])); ?>"><?php echo($thread['name']); ?></a></h4>
												<small><?php echo($thread['members']); ?> Members / <?php echo($thread['replies']); ?> Replies / Last reply by <a href="#"><?php echo($thread['started_by']); ?></a> <?php echo(pretty($thread['timestamp'])); ?></small>
											</div>
											<?php
											}
										}
										?>
									</div>
								</div>
							</div>
						<?php
						}
						?>
					</div>
					
					<nav class="text-center">
						<ul class="pagination">
							<li>
								<a href="#" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
					</nav>
					
				</div>
				
			</div><!-- container -->