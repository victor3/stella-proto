<?php
$values = explode(';',array_keys($_GET)[0]);
$size = explode('x',$values[0]);
$w = $size[0];
$h = (isset($size[1])) ? $size[1] : $size[0];
$text = ($values[2]!="") ? str_replace('+',' ',$values[2]) : $w.'x'.$h;

if (($values[0]=='avatar') | ($values[1]=='avatar')) {
	// AVATAR CODE /////////////////////////////////////////////////////////
	// create image
	$my_img = imagecreatetruecolor( $w, $h );
	// create source
	$file = 'avatar-01.png';
	$src_img = imagecreatefromjpeg($file);
	list($src_w,$src_h) = getimagesize($file);
	// resize image from source
	imagecopyresized($my_img, $src_img, 0, 0, 0, 0, $w, $h, $src_w, $src_h);
	// output image
	header( "Content-type: image/jpeg" );
	imagejpeg( $my_img );
	
} else {
	// LABELED ICON CODE /////////////////////////////////////////////////////////
	$rgb = ($values[1]!="") ? hex2rgb($values[1]) : array( 90, 90, 90 );
	$my_img = imagecreate( $w, $h );
	$background = imagecolorallocate( $my_img, $rgb[0], $rgb[1], $rgb[2] );
	$text_colour = imagecolorallocate( $my_img, 255, 255, 255 );
	// font size
	$f = $w/20;
	// position text
	$font_width = ImageFontWidth($f);
	$font_height = ImageFontHeight($f);
	$text_width = $font_width * strlen($text);
	$position_center = ceil(($w - $text_width) / 2); // Position to align in center
	$text_height = $font_height;
	$position_middle = ceil(($h - $text_height) / 2);
	// output text
	imagestring( $my_img, $f, $position_center, $position_middle, $text, $text_colour );
	// output image
	header( "Content-type: image/jpeg" );
	imagejpeg( $my_img );
	// clean up
	imagecolordeallocate( $text_color );
	imagecolordeallocate( $background );
	imagedestroy( $my_img );
}

// FUNCTIONS
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}
?>