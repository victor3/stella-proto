/* JS Functions */

$(document).ready(function(){
	
	// experience slider
	var carousel = $("#carousel");
	carousel.owlCarousel({
		items : 6, //10 items above 1000px browser width
		itemsDesktop : [1000,5], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,3], // betweem 900px and 601px
		itemsTablet: [600,2], //2 items between 600 and 0
		itemsMobile: [430,1], //1 items below 430
		scrollPerPage : true,
		paginationNumbers : true,
		//addClassActive : true,
		//navigation : true,
	});
	$(".next-slide").click(function(){
		carousel.trigger('owl.next');
	});
	$(".prev-slide").click(function(){
		carousel.trigger('owl.prev');
	});
	
	
	// menu item actions
	$('.menu-item').click(function(){
		$('.menu-item.selected').removeClass('.selected')
		$(this).addClass('.selected')
	})
		
	// make conversations clickable
	$('.clickable').hover(
			function(){ $(this).css('background-color','rgba(255,255,255,0.5)') },
			function(){ $(this).css('background-color','rgba(255,255,255,0.0)') }
		)
		.css('cursor','pointer')
		.click(function(){
			console.log(this.id+' [clicked]')
			return false
		})
	
	// close alerts
	$('.close').click(function(){
		$(this).parent().fadeOut();
	})
	
	$('.resize').click(function(){
		$('#spark').toggleClass('wide');
	})

	$('#sparkicon').click(function(){
		//$('#spark').addClass('.on')
		$('#spark').css('right','10px')
		$('#sparkbox').fadeIn()
		$(this).fadeOut()
	});
	
	$('#sparkbox .close').click(function(){
		$('#sparkicon').fadeIn()
		$('#sparkbox').fadeOut()
		$('#spark').css('right','-1000px')
		//$('#spark').removeClass('on')
		//$('#sparkbox').removeClass('on')
	})
	
	// toggle user account menu
	$('#account-toggle').click(function(){
		$('#account-menu').toggleClass('open');
	});
	
	// edit wiki
	$('#wiki-edit-btn').click(function(){
		$('#wiki-edit').show();
		$('#wiki-edit-btn').hide();
		$('#wiki-html').hide();
	});
	// save wiki
	$('#wiki-save').click(function(){
		$('#wiki-edit').hide();
		$('#wiki-edit-btn').show();
		$('#wiki-html').show();
	});
	
	// progress bar popovers
	$('.progress-bar').popover({
		delay: { "show": 500, "hide": 0 },
		trigger: "hover",
	})
	
	// avatar images 
	$('img').each(function(){
		var src = $(this).attr('src');
		if (src.indexOf("avatar") >= 0) {
			var num = Math.round(Math.random() * (9 - 1) + 1);
			//$(this).attr('src', 'lib/img/avatar-0'+num+'.jpg');
			$(this).attr('src', 'lib/img/avatar.png')
		}
		if (src.indexOf("32") >= 0) {
			$(this).css('width','32')
			$(this).css('height','32')
		}
		if (src.indexOf("24") >= 0) {
			$(this).css('width','24')
			$(this).css('height','24')
		}
	});
	
	// reply area
	textarea_focus = false;
	$('#editor-container').click(function(){
		if (!textarea_focus) {
			$('#formatting-container').show('fast')
			$('#editor-wrapper').addClass('active')
			$('#editor-container').addClass('active')
			$(this).addClass('active')
			textarea_focus = true;
		}
	})
	
	// QUILL RICH TEXT EDITOR
	var editor = new Quill('#editor-container', {
	  modules: {
		'toolbar': { container: '#formatting-container' },
		'link-tooltip': true,
		'image-tooltip': true
	  }
	});
	editor.on('selection-change', function(range) {
		console.log('selection-change', range);
	});
	editor.on('text-change', function(delta, source) {
		console.log('text-change', delta, source);
	});
	$('#formatting-container').hide();
	
	
	// window resize functions
	$(window).resize(function(){
		$('#messages').html( 'width:'+$(window).width() +' height:'+ $(window).height() )
		
		// resize experience 
	});
});
	
// FUNCTIONS 

function showexp(id) { 
	// make item active
	$('.experience-item').removeClass('active');
	$('#experience-item-'+id).addClass('active');
	
	$('.progress-bar').removeClass('active');
	$('#exp-bar-'+id).addClass('active');
	
	// show experience page content
	$('#show-experience-page').find('.experience-wrapper').fadeOut('fast');
	$('#show-experience-page').find('#experience-wrapper-'+id).css('visibility','visible').fadeIn('slow');
}

function toggle(ref) {
	$('#'+ref+'-box').toggle('slow');
	$('#'+ref+'-button').toggle('slow');
}

function toggle_class(ref) {
	$('#'+ref+'-box').slideToggle('slow');//.toggleClass('hidden-item');
	$('#'+ref+'-button').slideToggle('slow');//.toggleClass('hidden-item');
	$('.'+ref+'-button-on').slideToggle('slow');//.toggleClass('hidden-item');
	$('.'+ref+'-button-off').slideToggle('slow');//.toggleClass('hidden-item');
}

function exp_header(id) {
	$('.experience-masthead').toggleClass('collapsed');
}

function show_section(id, cont) {
	$('#'+cont+'-menu').find('li').removeClass('active');
	$('#'+id+'-menu').addClass('active');
	
	$('#'+cont).find('.section').addClass('hidden');
	$('#'+id+'-box').removeClass('hidden');
}

function add_note(id) {
	message = $('#note-textarea-'+id).val();
	if (message != '') {
		var now = new Date();
		$('#notes-'+id).prepend('<div class="activity-item"><img class="img-circle" src="lib/img/?32x32;avatar" alt="icon"><div class="activity-content"><strong><a href="./?id=user&name=your_username">[Your Username]</a></strong><br>'+message+'<br><small>'+now+'</small><!--<br><a class="activity-action" href="#">Reply</a>--></div></div>');
	}
}

function add_member(id) {
	var rand = Math.round(Math.random(1,99999));
	var del = '&#x274c;';
	var member = $('#'+id+'-field').val();
	var member_id = member.replace(' ','-')+'-'+rand;
	if (member != '') {
		$('#'+id+'-container').append( '<span id="member-'+member_id+'" class="added-member">'+member+' <a class="delete-member" href="javascript:delete_member(\''+member_id+'\');">'+del+'</a></span>' );
	}
}

function delete_member(id) {
	$('#member-'+id).remove();
}

function edit(section, item) {
	if (section==='close') {
		$('#edit-modal').find('.edit-box').fadeOut('fast');//.addClass('hidden')
		$('#edit-modal').fadeOut('fast');//.addClass('hidden')
		return false
	} else {
		$('#edit-modal').fadeIn('fast');//.removeClass('hidden')
		$('#edit-modal').find('.edit-box').fadeOut();//.addClass('hidden')
		$('#edit-'+section+'-box').fadeIn('fast');//.removeClass('hidden')
		if (item !== '') {
			$('#edit-'+section+'-box h3 span').html('Edit')
		} else {
			$('#edit-'+section+'-box h3 span').html('New')
		}
	}
}

function del() {
	confirm('Are you sure you want to delete this item?')
}

// function pretty(time)
// {
// 	var date = new Date();
// 	var etime = date - time.getTime();
// 	var future = false;
//
// 	if (etime < 1)
// 	{
// 		etime = time.getTime() - date;//'0 seconds';
// 		future = true;
// 	}
//
	// var a = { 365 * 24 * 60 * 60 :	'year',
	// 		   30 * 24 * 60 * 60 :	'month',
	// 				24 * 60 * 60 :	'day',
	// 					 60 * 60 :	'hour',
	// 						  60 :	'minute',
	// 						   1 :	'second'
	// };
	// var a_plural = {   'year':	'years',
	// 				  'month':	'months',
	// 					'day':	'days',
	// 				   'hour':	'hours',
	// 				 'minute':	'minutes',
	// 				 'second':	'seconds'
	// };
	//
	// var ago = (future) ? '' : ' ago';
	// var in = (future) ? 'in ' : '';
	//
	// $.each(a, function( secs, str ))
	// {
	// 	var d = etime / secs;
	// 	if (d >= 1)
	// 	{
	// 		r = Math.round(d);
	// 		return in + r + ' ' + (r > 1 ? a_plural[str] : str) . ago;
	// 	}
	// }
// }




